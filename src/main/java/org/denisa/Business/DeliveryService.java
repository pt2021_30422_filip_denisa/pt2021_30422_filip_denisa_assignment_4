package org.denisa.Business;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.denisa.Business.Exceptions.InvalidDataException;
import org.denisa.Business.Validators.MenuItemValidator;
import org.denisa.Business.Validators.UserValidator;
import org.denisa.DataAccess.CsvReader;
import org.denisa.DataAccess.PDFWriter;
import org.denisa.DataAccess.Serializer.CustomDeserializer.OrderKeyDeserializer;
import org.denisa.DataAccess.Serializer.MenuItemSerializer;
import org.denisa.DataAccess.Serializer.OrderSerializer;
import org.denisa.Model.Products.CompositeProduct;
import org.denisa.Model.Products.MenuItem;
import org.denisa.Model.Order.Order;
import org.denisa.Model.User.Client;

import java.awt.*;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.*;
import java.beans.PropertyChangeSupport;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * <p>DeliveryService is a class that implements the IDeliveryServiceProcessing interface</p>
 *
 * @invariant isWellFormed()
 *
 * @author Denisa Filip
 */
@Setter
@Getter
@AllArgsConstructor
public class DeliveryService implements IDeliveryServiceProcessing, Observable {
    @JsonProperty("map")
    @JsonDeserialize(keyUsing = OrderKeyDeserializer.class)
    private Map<Order, List<MenuItem>> orderInformation; //list because a client might order two or more portions of the same dish
    private Set<MenuItem> menu; //set because we don't want to have duplicates in the menu
    private final PropertyChangeSupport support = new PropertyChangeSupport(this); //for Observer design pattern

    @JsonCreator
    public DeliveryService() {
        orderInformation = new LinkedHashMap<>();
        orderInformation = new OrderSerializer().deserializeMap();
        menu = new LinkedHashSet<>();
        assert isWellFormed();
    }

    //administrator
    @Override
    public Set<MenuItem> importProducts() {
        assert isWellFormed();

        CsvReader csvReader = new CsvReader();
        menu = csvReader.processCSVFile("D:\\Documents\\UTCN\\Anul II\\SEM2\\PT\\PT2021_30422_Filip_Denisa_Assignment_4\\PT2021_30422_Filip_Denisa_Assignment_4\\src\\main\\java\\org\\denisa\\products.csv");
        new MenuItemSerializer().serializeSet(menu);

        assert isWellFormed();
        return menu;
    }

    @Override
    public Set<MenuItem> importModifiedProducts() {
        assert isWellFormed();

        menu = new MenuItemSerializer().deserializeSet();

        assert isWellFormed();
        return menu;
    }

    public Set<MenuItem> getMenu() {
        assert isWellFormed();

        menu = new MenuItemSerializer().deserializeSet();

        assert isWellFormed();
        return menu;
    }

    @Override
    public void addMenuProduct(MenuItem menuItem) {
        assert isWellFormed();
        assert menuItem != null && !this.menu.contains(menuItem);

        int initialSize = this.menu.size();

        this.menu.add(menuItem);
        new MenuItemSerializer().serializeSet(menu);
        assert this.menu.size() == initialSize + 1;
        assert isWellFormed();
    }

    @Override
    public void removeMenuProduct(MenuItem menuItem) {
        assert isWellFormed();
        assert menuItem != null && this.menu.contains(menuItem);

        int initialSize = this.menu.size();

        this.menu.remove(menuItem);
        new MenuItemSerializer().serializeSet(menu);
        assert this.menu.size() == initialSize - 1;
        assert isWellFormed();
    }

    @Override
    public void createCompositeProduct(String title, LinkedHashSet<MenuItem> menuItems) {
        assert isWellFormed();
        assert title != null && !title.isEmpty() && menuItems != null && menuItems.size() >= 2;

        int initialSize = menu.size();

        addMenuProduct(new CompositeProduct(title, menuItems));
        new MenuItemSerializer().serializeSet(menuItems);

        assert menu.size() == initialSize + 1;
        assert isWellFormed();
    }

    @Override
    public Set<Order> generateTimeIntervalOfTheOrders(LocalTime startHour, LocalTime endHour) {
        assert isWellFormed();
        assert startHour != null && endHour != null && Duration.between(startHour, endHour).isNegative();
        return orderInformation.keySet().stream().filter(order -> order.getOrderDate().getHour() > startHour.getHour()).filter(order ->
                order.getOrderDate().getHour() < endHour.getHour()).collect(Collectors.toSet());
    }

    @Override
    public Set<MenuItem> generateMostOrderedProducts(int numberOfTimes) {
        assert isWellFormed();
        assert numberOfTimes >= 0;

        //merging the lists of the orderInformation values into a single big list
        List<MenuItem> newList = orderInformation.values().stream().flatMap(List::stream).collect(Collectors.toList());
        Map<MenuItem, Long> frequency = newList.stream().collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
        return frequency.entrySet().stream().filter(menuItemLongEntry -> menuItemLongEntry.getValue() >= numberOfTimes).map(Map.Entry::getKey).
                collect(Collectors.toSet());
    }

    @Override
    public Set<Client> generateMostLoyalCustomers(int noOfTimes, double specifiedAmount) {
        assert isWellFormed();
        assert noOfTimes > 0 && specifiedAmount > 0.0;

        Map<Client, Long> frequency = orderInformation.keySet().stream().filter(order -> order.getTotalPrice() >= specifiedAmount).
                collect(Collectors.groupingBy(Order::getClient, Collectors.counting()));
        return frequency.entrySet().stream().filter(clientLongEntry -> clientLongEntry.getValue() >= noOfTimes).map(Map.Entry::getKey).
                collect(Collectors.toSet());
    }

    @Override
    public Map<MenuItem, Long> generateProductsOrderedWithinASpecificDay(LocalDate specifiedDate) {
        assert isWellFormed();
        assert specifiedDate.isBefore(LocalDate.now());

        Map<Order, List<MenuItem>> orderListMap = orderInformation.entrySet().stream().filter(map -> map.getKey().getOrderDate().toLocalDate().
                equals(specifiedDate)).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
        List<MenuItem> menuItemList = orderListMap.values().stream().flatMap(List::stream).collect(Collectors.toList());

        assert isWellFormed();
        return menuItemList.stream().collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
    }

    public void removeMenuItemFromOrder(Order order, MenuItem menuItem) {
        //handle exceptions
        assert isWellFormed();
        orderInformation.get(order).remove(menuItem);
        assert isWellFormed();
    }

    public void removeAllItemsFromOrder(Order order) {
        assert isWellFormed();
        orderInformation.get(order).removeAll(orderInformation.get(order));
        assert isWellFormed();
    }

    public void addMenuItemToOrder(Order order, MenuItem menuItem) {
        assert isWellFormed();
        orderInformation.get(order).add(menuItem);
        assert isWellFormed();
    }

    @Override
    public void placeOrder(Client client, List<MenuItem> orderedMenuItems) throws IOException {
        assert isWellFormed();
        assert client != null && orderedMenuItems != null && !orderedMenuItems.isEmpty();

        int initialSize = orderInformation.size();

        Order order = new Order(client);

        order.computeTotalPriceOfOrder(orderedMenuItems);
        orderInformation.put(order, orderedMenuItems);

        support.firePropertyChange("placed order", null, orderInformation.get(order));
        support.firePropertyChange("client", null, order);

        PDFWriter pdfWriter = new PDFWriter(order, orderInformation.get(order));
        pdfWriter.writeBillToPDF();

        OrderSerializer orderSerializer = new OrderSerializer();
        orderSerializer.serializeMap(orderInformation);

        if (Desktop.isDesktopSupported()) {
            File file = new File("D:\\Documents\\UTCN\\Anul II\\SEM2\\PT\\PT2021_30422_Filip_Denisa_Assignment_4\\PT2021_30422_Filip_Denisa_Assignment_4\\src\\main\\resources\\org\\denisa\\pdfs\\bill" + order.getOrderID() + ".pdf");
            Desktop.getDesktop().open(file);
        }

        assert orderInformation.size() == initialSize + 1;
        assert isWellFormed();
    }

    @Override
    public LinkedHashSet<MenuItem> searchByCriteria(MenuItem menuItem) {
        assert isWellFormed();
        assert menuItem != null;

        Predicate<MenuItem> condition = item -> {
            if (!menuItem.getTitle().equals("null") && (!item.getTitle().equals(menuItem.getTitle()) && !item.getTitle().contains(menuItem.getTitle()))) return false;
            if (!menuItem.getRating().equals(-1.0) && !item.getRating().equals(menuItem.getRating())) return false;
            if (!menuItem.getCalories().equals(-1) && !item.getCalories().equals(menuItem.getCalories())) return false;
            if (!menuItem.getProtein().equals(-1.0) && !item.getProtein().equals(menuItem.getProtein())) return false;
            if (!menuItem.getFat().equals(-1.0) && !item.getFat().equals(menuItem.getFat())) return false;
            if (!menuItem.getSodium().equals(-1.0) && !item.getSodium().equals(menuItem.getSodium())) return false;
            if (menuItem.getPrice() != -1.0 && item.getPrice().equals(menuItem.getPrice())) return false;
            return true;
        };
        assert isWellFormed();
        return menu.stream().filter(condition).collect(Collectors.toCollection(LinkedHashSet::new));
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        support.addPropertyChangeListener(listener);
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        support.removePropertyChangeListener(listener);
    }

    @Override
    public boolean isWellFormed() {
        if (orderInformation == null || menu == null) {
            return false;
        } else {
            if (!orderInformation.isEmpty()) {
                List<MenuItem> orderedMenuItems = orderInformation.values().stream().flatMap(List::stream).collect(Collectors.toList());
                if (!orderedMenuItems.isEmpty()) {
                    for (MenuItem menuItem : orderedMenuItems) {
                        try {
                            new MenuItemValidator().validate(menuItem);
                        } catch (InvalidDataException e) {
                            return false;
                        }
                    }
                }
                Map<Client, Long> clients = orderInformation.keySet().stream().collect(Collectors.groupingBy(Order::getClient, Collectors.counting()));
                if (!clients.isEmpty()) {
                    for (Client client : clients.keySet()) {
                        try {
                            new UserValidator().validate(client);
                        } catch (InvalidDataException e) {
                            return false;
                        }
                    }
                }
            }

        }
        return true;
    }
}
