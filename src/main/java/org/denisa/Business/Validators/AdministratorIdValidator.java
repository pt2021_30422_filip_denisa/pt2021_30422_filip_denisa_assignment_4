package org.denisa.Business.Validators;

import org.denisa.Business.Exceptions.InvalidDataException;
import org.denisa.Model.User.Administrator;

public class AdministratorIdValidator implements Validator<Administrator> {

    @Override
    public void validate(Administrator admin) throws InvalidDataException {
        final String ADMIN_ID = "A_" + admin.getFirstName().charAt(0) + "_" + admin.getLastName().charAt(0);
        if (admin.getAdministratorId() != null && !admin.getAdministratorId().equals(ADMIN_ID)) {
            throw new InvalidDataException("The inserted id is not valid.");
        }

    }
}
