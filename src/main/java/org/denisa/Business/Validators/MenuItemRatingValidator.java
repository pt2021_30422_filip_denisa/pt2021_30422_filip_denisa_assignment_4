package org.denisa.Business.Validators;

import org.denisa.Business.Exceptions.InvalidDataException;
import org.denisa.Model.Products.MenuItem;

public class MenuItemRatingValidator implements Validator<MenuItem> {
    @Override
    public void validate(MenuItem menuItem) throws InvalidDataException {
        if (menuItem.getRating() != null  && menuItem.getRating() < 0.0) {
            throw new InvalidDataException("The rating of the product must be a positive number.");
        }
    }
}
