package org.denisa.Business.Validators;

import org.denisa.Business.Exceptions.InvalidDataException;
import org.denisa.Model.User.Employee;
import org.denisa.Model.User.User;

public class EmployeeIdValidator implements Validator<Employee> {

    @Override
    public void validate(Employee employee) throws InvalidDataException {
        final String EMP_ID = "E_" + employee.getFirstName().charAt(0) + "_" + employee.getLastName().charAt(0);
        if (employee.getEmployeeId() != null && !employee.getEmployeeId().equals(EMP_ID)) {
            throw new InvalidDataException("The inserted id is not valid.");
        }

    }
}
