package org.denisa.Business.Validators;

import org.denisa.Business.Exceptions.InvalidDataException;
import org.denisa.Model.Products.MenuItem;

public class MenuItemNutritionalDataValidator implements Validator<MenuItem> {
    @Override
    public void validate(MenuItem menuItem) throws InvalidDataException {
        if (menuItem.getCalories() != null && menuItem.getCalories() < 0) {
            throw new InvalidDataException("The number of calories must be a positive number.");
        }
        if (menuItem.getProtein() != null && menuItem.getProtein() < 0.0) {
            throw new InvalidDataException("The protein quantity must be a positive number.");
        }
        if (menuItem.getFat() != null && menuItem.getFat() < 0) {
            throw new InvalidDataException("The fat quantity must be a positive number.");
        }
        if (menuItem.getSodium() != null && menuItem.getSodium() < 0) {
            throw new InvalidDataException("The sodium quantity must be a positive number.");
        }
    }
}
