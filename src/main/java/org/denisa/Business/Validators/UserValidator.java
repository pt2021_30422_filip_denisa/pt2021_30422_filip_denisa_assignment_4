package org.denisa.Business.Validators;

import lombok.Getter;
import lombok.Setter;
import org.denisa.Business.Exceptions.DuplicateEmailException;
import org.denisa.Business.Exceptions.InvalidDataException;
import org.denisa.Model.User.Administrator;
import org.denisa.Model.User.Employee;
import org.denisa.Model.User.User;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class UserValidator implements Validator<User>{
    private List<Validator<User>> validators;

    /**
     * <p>Constructor without arguments for the UserValidator class. It initializes the validators list.</p>
     */
    public UserValidator() {
        validators = new ArrayList<>();
        validators.add(new UserNameValidator());
        validators.add(new UserEmailValidator());
        validators.add(new UserPasswordValidator());
    }

    @Override
    public void validate(User user) throws InvalidDataException {
        if (user instanceof Employee) {
            new EmployeeIdValidator().validate((Employee) user);
        } else if (user instanceof Administrator) {
            new AdministratorIdValidator().validate((Administrator) user);
        }
        for (Validator<User> validator : validators) {
            validator.validate(user);
        }
    }

    /**
     * Function to check if an email was already registered in the internal database of the application.
     * @param user whose email is checked;
     * @param users list, containing all the users registered in the application up to the present moment;
     * @throws DuplicateEmailException if the email of the user was already registered.
     */
    public void checkForDuplicateEmail(User user, List<User> users) throws DuplicateEmailException {
        if (users == null) {
            return;
        }
        for (User u : users) {
            if (user.getEmail().equals(u.getEmail())) {
                throw new DuplicateEmailException("This email already has an account on this platform. Please go to sign in or use another email.");
            }
        }
    }

    /**
     * Function that serves as utility for the sign in functionality. It checks if an user was already registered in the internal database of the application,
     * with the email and password combination.
     * @param email of the user
     * @param password of the user
     * @param users list, containing all the users registered in the application up to the present moment;
     * @return the user data if they were already registered | null otherwise
     */
    public User checkIfUserExists(String email, String password, List<User> users) {
        if (users == null) {
            return null;
        }
        for (User u : users) {
            if (email.equals(u.getEmail()) && password.equals(u.getPassword())) {
                return u;
            }
        }
        return null;
    }
}
