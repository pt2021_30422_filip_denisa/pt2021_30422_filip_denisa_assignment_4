package org.denisa.Business.Validators;

import org.denisa.Business.Exceptions.InvalidDataException;
import org.denisa.Model.Products.MenuItem;

public class MenuItemTitleValidator implements Validator<MenuItem> {
    private static final String TITLE_PATTERN = "([A-Z][a-z0-9]*\\s?[-]?[&]?\\s?)+";

    @Override
    public void validate(MenuItem menuItem) throws InvalidDataException {
        if (menuItem.getTitle() != null && !menuItem.getTitle().matches(TITLE_PATTERN)) {
            throw new InvalidDataException("The title of the product is invalid.");
        }
    }
}
