package org.denisa.Business.Validators;

import org.denisa.Business.Exceptions.InvalidDataException;
import org.denisa.Model.User.User;

public class UserPasswordValidator implements Validator<User> {
    private static final String PASSWORD_REGEX = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[-_@$!%*?&])[A-Za-z\\d@_$!%*?&]{6,}$";
    @Override
    public void validate(User user) throws InvalidDataException {
        if (user.getPassword() != null && !user.getPassword().matches(PASSWORD_REGEX)) {
            throw new InvalidDataException("The password must have at least 6 characters, an uppercase letter, a lowercase letter, " +
                    "a special character and a digit.");
        }
    }
}
