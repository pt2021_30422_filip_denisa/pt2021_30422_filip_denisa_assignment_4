package org.denisa.Business.Validators;

import org.denisa.Business.Exceptions.InvalidDataException;

/**
 * Validator is a parameterized interface responsible for validating the user input.
 *
 * @param <T> - the parameter can be any of the Presentation.Model classes - Client, Product, Supply, Warehouse, PlacedOrder
 * @author Denisa Filip
 * @version $Id: $Id
 */
public interface Validator<T> {

    /**
     * validate is the function that verifies if a field of a model class is correct and valid. If it isn't, it throws a IllegalArgumentException
     * exception.
     *
     * @param t - object whose fields are validated.
     * @throws InvalidDataException if the fields of the object t contain invalid data.
     */
    void validate(T t) throws InvalidDataException;
}
