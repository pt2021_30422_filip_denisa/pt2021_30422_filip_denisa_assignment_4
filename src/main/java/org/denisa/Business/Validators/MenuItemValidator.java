package org.denisa.Business.Validators;

import org.denisa.Business.Exceptions.DuplicateMenuItemTitleException;
import org.denisa.Business.Exceptions.InvalidDataException;
import org.denisa.Model.Products.MenuItem;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class MenuItemValidator implements Validator<MenuItem> {

    private List<Validator<MenuItem>> validators;

    /**
     * <p>Constructor without arguments for the MenuItemValidator class. It initializes the validators list.</p>
     */
    public MenuItemValidator() {
        validators = new ArrayList<>();
        validators.add(new MenuItemTitleValidator());
        validators.add(new MenuItemRatingValidator());
        validators.add(new MenuItemNutritionalDataValidator());
        validators.add(new MenuItemPriceValidator());
    }

    @Override
    public void validate(MenuItem menuItem) throws InvalidDataException {
        for (Validator<MenuItem> v : validators) {
            v.validate(menuItem);
        }
    }

    public void checkForDuplicateTitle(MenuItem menuItem, Set<MenuItem> menuItemsSet) throws DuplicateMenuItemTitleException {
        for (MenuItem item : menuItemsSet) {
            if (item.getTitle().equals(menuItem.getTitle())) {
                throw new DuplicateMenuItemTitleException("This menu item already exists.");
            }
        }
    }
}
