package org.denisa.Business.Validators;

import org.denisa.Business.Exceptions.InvalidDataException;
import org.denisa.Model.User.User;

import java.util.regex.Pattern;

public class UserNameValidator implements Validator<User> {
    private static final String NAME_PATTERN = "[a-zA-Z- ăîâșț]+";

    /** {@inheritDoc} */
    @Override
    public void validate(User user) throws InvalidDataException {
        Pattern pattern = Pattern.compile(NAME_PATTERN);
        if (user.getFirstName() != null && !pattern.matcher(user.getFirstName()).matches()) {
            throw new InvalidDataException("First name is not a valid first name!");
        }
        if (user.getLastName() != null && !pattern.matcher(user.getLastName()).matches()) {
            throw new InvalidDataException("Last name is not a valid last name!");
        }
    }

}
