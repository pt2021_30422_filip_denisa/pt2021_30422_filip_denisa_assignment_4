package org.denisa.Business.Validators;

import org.denisa.Business.Exceptions.InvalidDataException;
import org.denisa.Model.Products.MenuItem;

public class MenuItemPriceValidator implements Validator<MenuItem> {
    @Override
    public void validate(MenuItem menuItem) throws InvalidDataException {
        if (menuItem.getPrice() != null  && menuItem.getPrice() < 0) {
            throw new InvalidDataException("The price of the product must be a positive number.");
        }
    }
}
