package org.denisa.Business;

import org.denisa.Model.Products.MenuItem;
import org.denisa.Model.Order.Order;
import org.denisa.Model.User.Client;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * IDeliveryServiceProcessing is an interface that defines the actions a client and an administrator can do in the food delivery management
 * system. It also notifies an Employee each time an order is placed, with the help of the Observer design pattern.
 *
 * @invariant isWellFormed()
 *
 * @author Denisa Filip
 */
public interface IDeliveryServiceProcessing {
    /**
     * Imports the initial menu items from the products.csv file and overrides the current menu items.
     * Action performed by the administrator of the system.
     * @return a set with the imported menu items
     *
     * @post menu != null
     */
    Set<MenuItem> importProducts();

    /**
     * Imports the updated menu items from the MenuItem.json file. Action performed by the administrator of the system.
     * @return a set with the imported and modified menu items
     */
    Set<MenuItem> importModifiedProducts();

    /**
     * Function that adds a new menu item to the set of the menu items that compose the menu of the catering company.
     * @param menuItem to be added to the menu
     *
     * @pre menuItem != null
     * @pre !this.menu.contains(menuItem)
     *
     * @post menu.size() == menu.initialSize() + 1
     */
    void addMenuProduct(MenuItem menuItem);

    /**
     * Function that removes an existing menu item from the set of the menu items that compose the menu of the catering company.
     * @param menuItem to be added to the menu
     * @pre menuItem != null
     * @pre this.menu.contains(menuItem)
     *
     * @post  menu.size() == menu.initialSize() - 1
     */
    void removeMenuProduct(MenuItem menuItem);

    /**
     * Function that creates a new composite product from a specified title and list of menu items.
     * @param title of the newly created composite product;
     * @param menuItems of the newly created composite product.
     *
     * @pre title != null && !title.isEmpty()
     * @pre menuItems != null && menuItems.size() >= 2
     *
     * @post  menu.size() == menu.initialSize() + 1
     */
    void createCompositeProduct(String title, LinkedHashSet<MenuItem> menuItems);

    /**
     * Function that generates a report with the orders performed between a given start hour and a given end hour, regardless of the date.
     * @param startHour from which orders are taken into consideration;
     * @param endHour until which orders are taken into consideration;
     * @return a set with the orders that were performed between the start hour and end hour, regardless of the date.
     *
     * @pre  startHour < endHour
     * @pre  startHour != null
     * @pre  endHour != null
     */
    Set<Order> generateTimeIntervalOfTheOrders(LocalTime startHour, LocalTime endHour);

    /**
     * Generates a report with the products ordered more than a specified number of times so far.
     * @param numberOfTimes a product must have been ordered
     * @return a set with the products that have been ordered more than the specified number of times so far.
     *
     * @pre  numberOfTimes >= 0
     */
    Set<MenuItem> generateMostOrderedProducts(int numberOfTimes);

    /**
     * Generates a report with the clients that have ordered more than a specified number of times and the value of the order was
     * higher than a specified amount.
     * @param noOfTimes represents the minimum number of times a client must have ordered from the catering company
     * @param specifiedAmount represents the minimum value of an order
     * @return a set with the clients that satisfy the given requirements.
     *
     * @pre  noOfTimes > 0
     * @pre  specifiedAmount >= 0.0
     */
    Set<Client> generateMostLoyalCustomers(int noOfTimes, double specifiedAmount);

    /**
     * Generates a report with the products ordered within a specified day and the number of times they have been ordered.
     * @param specifiedDate represents the given day in which a product must have been ordered
     * @return a map with the menu items that have been ordered in the given day (key) and the number of times they have been ordered (value).
     *
     * @pre specifiedDate != null
     * @pre specifiedDate <= LocalDate.now()
     */
    Map<MenuItem, Long> generateProductsOrderedWithinASpecificDay(LocalDate specifiedDate);

    /**
     * Function that is responsible for placing an order, by performing the following actions:
     *      <ul>
     *          <li>Creates a new order in the name of the client, given as parameter.</li>
     *          <li>Computes the total price of the order, by summing up the prices of the ordered menu items, given as parameter.</li>
     *          <li>Notifies the employees that a new order has been placed, so that they can prepare it.</li>
     *          <li>Writes the bill of the order to a PDF file.</li>
     *          <li>Opens the bill in PDF format on the computer of the user.</li>
     *      </ul>
     * @param client that placed a new order;
     * @param orderedMenuItems that the client bought from the catering company;
     * @throws IOException if the bill of the order was not found
     *
     * @pre  client != null
     * @pre orderedMenuItems != null && !orderedMenuItems.isEmpty()
     *
     * @post  orderInformation.size() == orderInformation.initialSize() + 1
     */
    void placeOrder(Client client, List<MenuItem> orderedMenuItems) throws IOException;

    /**
     * Function that searches the menu of the catering company by user inserted criteria, using Java Streams and Predicate.
     * @param menuItem that contains the criteria by which the menu must be filtered
     * @return a LinkedHashSet of MenuItems containing the menu items from the initial menu of the catering company that meet the given criteria
     *
     * @pre menuItem != null
     */
    LinkedHashSet<MenuItem> searchByCriteria(MenuItem menuItem);

    /**
     * The isWellFormed function checks a few conditions that ensure the fact that the interface IDeliveryServiceProcessing is well-formed.
     * The requirements for the interface to be well-formed are:
     *      <ol>
     *          <li>orderInformation != null && menu != null</li>
     *          <li>the ordered menu items (all of the menu items that have been ordered by clients) are valid</li>
     *          <li>the clients that processed orders from the catering company are valid</li>
     *      </ol>
     * @return true if all the conditions are met
     */
    boolean isWellFormed();
}
