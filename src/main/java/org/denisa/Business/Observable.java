package org.denisa.Business;

import java.beans.PropertyChangeListener;

/**
 * Observable interface is used for implementing the Observer design pattern. The classes that implement this interface are observables.
 * Created with the help of PropertyChangeListener and PropertyChangeSupport.
 *
 * @author Denisa Filip
 */
public interface Observable {

    /**
     * Function that adds a property change listener to an object of type PropertyChangeSupport, implemented by a class the implements this interface.
     * @param listener to be added to the support object
     */
    void addPropertyChangeListener(PropertyChangeListener listener);

    /**
     * Function that removes a property change listener from an object of type PropertyChangeSupport, implemented by a class the implements this interface.
     * @param listener to be removed from the support object
     */
    void removePropertyChangeListener(PropertyChangeListener listener);
}
