package org.denisa.Business.Exceptions;

/**
 * DuplicateEmailException is an exception that is thrown when an user tries to create a new account with an email that already
 * exists into the internal database.
 *
 * @author Denisa Filip
 * @version $Id: $Id
 */
public class DuplicateEmailException extends Exception {

    /**
     * This constructor is called when the exception is thrown, displaying a detailed diagnosis of the error.
     *
     * @param message containing the details of the error
     */
    public DuplicateEmailException(String message) {
        super(message);
    }
}
