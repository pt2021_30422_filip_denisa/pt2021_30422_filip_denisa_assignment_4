package org.denisa.Business.Exceptions;

/**
 * DuplicateMenuItemTitleException is an exception that is thrown when an administrator tries to insert a new menu item into the menu
 * with a title that already exists.
 *
 * @author Denisa Filip
 * @version $Id: $Id
 */
public class DuplicateMenuItemTitleException extends Exception {
    /**
     * This constructor is called when the exception is thrown, displaying a detailed diagnosis of the error.
     *
     * @param message containing the details of the error
     */
    public DuplicateMenuItemTitleException(String message) {
        super(message);
    }
}
