package org.denisa.Presentation;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import javafx.fxml.FXML;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.control.TreeTableView;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import org.denisa.Model.Order.Order;
import org.denisa.Model.Products.MenuItem;
import org.denisa.Model.User.Employee;
import org.denisa.Model.User.User;
import org.denisa.Presentation.TreeTableView.TreeTableViewController;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;

public class EmployeeController implements PropertyChangeListener {

    private Employee user;
    @FXML private JFXButton btnLogOut;
    @FXML private AnchorPane anchorPaneInfo;
    @FXML private AnchorPane anchorPaneOrderInfo;
    @FXML private JFXTextField txtClientName;

    @FXML private TreeTableView<MenuItem> treeTableView;
    @FXML private TreeTableColumn<MenuItem, String> titleProductColumn;
    @FXML private TreeTableColumn<MenuItem, Double> ratingProductColumn;
    @FXML private TreeTableColumn<MenuItem, Integer> caloriesProductColumn;
    @FXML private TreeTableColumn<MenuItem, Double> proteinProductColumn;
    @FXML private TreeTableColumn<MenuItem, Double> fatProductColumn;
    @FXML private TreeTableColumn<MenuItem, Double> sodiumProductColumn;
    @FXML private TreeTableColumn<MenuItem, Double> priceProductColumn;

    public void setEmployee(User user) {
        this.user = (Employee) user;
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        anchorPaneInfo.setVisible(false);
        anchorPaneOrderInfo.setVisible(false);
        anchorPaneInfo.setVisible(true);
        anchorPaneOrderInfo.setVisible(true);
        if (evt.getPropertyName().equals("client")) {
            Order order = (Order) evt.getNewValue();
            txtClientName.setText(order.getClient().getFirstName() + " " + order.getClient().getLastName());
        }
        if (evt.getPropertyName().equals("placed order")) {
            List<MenuItem> menuItems = (ArrayList<MenuItem>) evt.getNewValue();
            new TreeTableViewController().initializeTreeTableViewColumns(treeTableView, titleProductColumn,
                    ratingProductColumn, caloriesProductColumn, proteinProductColumn, fatProductColumn, sodiumProductColumn, priceProductColumn);
            new TreeTableViewController().importProducts(treeTableView, menuItems);
        }

    }

    @FXML
    private void logOut() {
        closeWindow();
    }

    private void closeWindow() {
        Stage stage = (Stage) btnLogOut.getScene().getWindow();
        stage.close();
    }
}
