package org.denisa.Presentation;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Line;
import javafx.stage.Stage;
import javafx.util.Callback;
import org.denisa.Business.DeliveryService;
import org.denisa.Model.Order.Order;
import org.denisa.Model.Products.MenuItem;
import org.denisa.Model.User.Client;
import org.denisa.Model.User.User;
import org.denisa.Presentation.TreeTableView.TreeTableViewController;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.*;
import java.util.List;

public class ClientController implements Initializable {
    private final Integer ADDING_ITEM_TO_CART = 1;
    private final Integer REMOVING_ITEM_FROM_CART = -1;
    @FXML private TreeTableView<MenuItem> treeTableView;
    @FXML private TreeTableColumn<MenuItem, String> titleColumn;
    @FXML private TreeTableColumn<MenuItem, Double> ratingColumn;
    @FXML private TreeTableColumn<MenuItem, Integer> caloriesColumn;
    @FXML private TreeTableColumn<MenuItem, Double> proteinColumn;
    @FXML private TreeTableColumn<MenuItem, Double> fatColumn;
    @FXML private TreeTableColumn<MenuItem, Double> sodiumColumn;
    @FXML private TreeTableColumn<MenuItem, Double> priceColumn;
    @FXML private TreeTableColumn<MenuItem, String> selectColumn;
    @FXML private JFXButton searchButton;
    @FXML private VBox searchSection;
    @FXML private JFXTextField txtSearchTitle;
    @FXML private JFXTextField txtSearchRating;
    @FXML private JFXTextField txtSearchCalories;
    @FXML private JFXTextField txtSearchProtein;
    @FXML private JFXTextField txtSearchFat;
    @FXML private JFXTextField txtSearchSodium;
    @FXML private JFXTextField txtSearchPrice;
    @FXML private JFXTextField txtTotalPrice;
    @FXML private JFXTextField txtNoOfItems;
    @FXML private Line lineUpper, lineLower;
    @FXML private Label lblShoppingCart;

    @FXML private TreeTableView<MenuItem> treeTableViewCart;
    @FXML private TreeTableColumn<MenuItem, String> titleCartColumn;
    @FXML private TreeTableColumn<MenuItem, Double> ratingCartColumn;
    @FXML private TreeTableColumn<MenuItem, Integer> caloriesCartColumn;
    @FXML private TreeTableColumn<MenuItem, Double> proteinCartColumn;
    @FXML private TreeTableColumn<MenuItem, Double> fatCartColumn;
    @FXML private TreeTableColumn<MenuItem, Double> sodiumCartColumn;
    @FXML private TreeTableColumn<MenuItem, Double> priceCartColumn;
    @FXML private TreeTableColumn<MenuItem, String> removeCartColumn;

    private Client client;
    private DeliveryService deliveryService;

    public void setDeliveryService(DeliveryService deliveryService) {
        this.deliveryService = deliveryService;
        populateTreeTableView(deliveryService.importModifiedProducts());
        displayOrderedItemsInCart();
    }

    public void setClient(User client) {
        this.client = (Client) client;
    }

    private Callback<TreeTableColumn<MenuItem, String>, TreeTableCell<MenuItem, String>> createButtonInTreeTableView(String buttonText, Integer priceSign) {
        return new Callback<>() {
            @Override
            public TreeTableCell<MenuItem, String> call(TreeTableColumn<MenuItem, String> param) {
                return new TreeTableCell<>() {
                    final JFXButton btn = new JFXButton(buttonText);

                    @Override
                    protected void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                        } else {
                            btn.setOnAction(event -> {
                                TreeItem<MenuItem> treeItem = getTreeTableView().getTreeItem(getIndex());
                                MenuItem menuItem = treeItem.getValue();
                                updateCurrentPrice(priceSign*menuItem.getPrice());
                                updateNumberOfItems(priceSign);
                                if (priceSign.equals(REMOVING_ITEM_FROM_CART)) {
                                    removingMenuItemFromCart(treeItem);
                                } else {
                                    addingMenuItemToCart(menuItem);
                                }
                            });
                            setGraphic(btn);
                        }
                        setText(null);
                    }
                };
            }
        };
    }

    private void addingMenuItemToCart(MenuItem menuItem) {
        treeTableViewCart.getRoot().getChildren().add(new TreeItem<>(menuItem));
    }

    private void removingMenuItemFromCart(TreeItem<MenuItem> treeItem) {
        treeTableViewCart.getRoot().getChildren().remove(treeItem);
    }

    private void displayOrderedItemsInCart() {
        new TreeTableViewController().initializeTreeTableViewColumns(treeTableViewCart, titleCartColumn, ratingCartColumn, caloriesCartColumn, proteinCartColumn, fatCartColumn, sodiumCartColumn, priceCartColumn);
        removeCartColumn.setCellFactory(createButtonInTreeTableView("Remove", REMOVING_ITEM_FROM_CART));

        TreeItem<MenuItem> baseProducts = new TreeItem<>();

        treeTableViewCart.setRoot(baseProducts);
        treeTableViewCart.setShowRoot(false);
    }

    private void populateTreeTableView(Set<MenuItem> menuItems) {
        new TreeTableViewController().initializeTreeTableViewColumns(treeTableView, titleColumn, ratingColumn, caloriesColumn, proteinColumn, fatColumn, sodiumColumn, priceColumn);
        selectColumn.setCellFactory(createButtonInTreeTableView("Add", ADDING_ITEM_TO_CART));

        new TreeTableViewController().importProducts(treeTableView, menuItems);
    }

    private void updateCurrentPrice(Double price) {
        Double currentPrice = 0.0;
        if (!txtTotalPrice.getText().isEmpty()) {
            currentPrice = Double.valueOf(txtTotalPrice.getText());
        }
        currentPrice += price;
        txtTotalPrice.setText(String.valueOf(currentPrice));
    }

    private void updateNumberOfItems(Integer amount) {
        Integer currentNoOfItems = 0;
        if (!txtNoOfItems.getText().isEmpty()) {
            currentNoOfItems = Integer.valueOf(txtNoOfItems.getText());
        }
        currentNoOfItems += amount;
        txtNoOfItems.setText(String.valueOf(currentNoOfItems));
    }

    @FXML
    private void searchByCriteria() {
        MenuItem menuItem = new SearchController().constructMenuItemFromSearch(txtSearchTitle, txtSearchRating, txtSearchCalories, txtSearchProtein,
                txtSearchFat, txtSearchSodium, txtSearchPrice);
        if (menuItem == null) {
            populateTreeTableView(deliveryService.importModifiedProducts());
        } else {
            Set<MenuItem> filteredMenuItems = deliveryService.searchByCriteria(menuItem);
            populateTreeTableView(filteredMenuItems);
        }
    }

    @FXML
    private void viewCart() {
        treeTableViewCart.setVisible(true);
        lineLower.setVisible(true);
        lineUpper.setVisible(true);
        lblShoppingCart.setVisible(true);
    }

    @FXML
    private void removeSelectedProducts() {
        treeTableViewCart.getRoot().getChildren().removeAll(treeTableViewCart.getRoot().getChildren());
        txtTotalPrice.setText("0.0");
        txtNoOfItems.setText("0");
    }

    @FXML
    private void placeOrder() {
        List<MenuItem> orderedMenuItems = new ArrayList<>();
        for (TreeItem<MenuItem> menuItemTreeItem : treeTableViewCart.getRoot().getChildren()) {
            orderedMenuItems.add(menuItemTreeItem.getValue());
        }
        try {
            deliveryService.placeOrder(client, orderedMenuItems);
            closeWindow();
        } catch (IOException e) {
            App.createAlert("Could not open bill in PDF format.");
        }
        closeWindow();
    }

    private void closeWindow() {
        Stage stage = (Stage) searchButton.getScene().getWindow();
        stage.close();
    }

    @FXML
    private void logOut() {
        closeWindow();
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

    }
}
