package org.denisa.Presentation.TreeTableView;

import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.control.TreeTableView;
import javafx.scene.control.cell.TextFieldTreeTableCell;
import javafx.scene.control.cell.TreeItemPropertyValueFactory;
import javafx.util.converter.DoubleStringConverter;
import org.denisa.Business.DeliveryService;
import org.denisa.Model.Products.BaseProduct;
import org.denisa.Model.Products.CompositeProduct;
import org.denisa.Model.Products.MenuItem;
import org.denisa.Presentation.App;

import java.util.Collection;
import java.util.Set;

public class TreeTableViewController {

    public void initializeTreeTableViewColumns(TreeTableView<MenuItem> treeTableView, TreeTableColumn<MenuItem, String> titleColumn,
                                                  TreeTableColumn<MenuItem, Double> ratingColumn, TreeTableColumn<MenuItem, Integer> caloriesColumn,
                                                  TreeTableColumn<MenuItem, Double> proteinColumn, TreeTableColumn<MenuItem, Double> fatColumn,
                                                  TreeTableColumn<MenuItem, Double> sodiumColumn, TreeTableColumn<MenuItem, Double> priceColumn) {
        titleColumn.setCellValueFactory(new TreeItemPropertyValueFactory<>("title"));
        ratingColumn.setCellValueFactory(new TreeItemPropertyValueFactory<>("rating"));
        caloriesColumn.setCellValueFactory(new TreeItemPropertyValueFactory<>("calories"));
        proteinColumn.setCellValueFactory(new TreeItemPropertyValueFactory<>("protein"));
        fatColumn.setCellValueFactory(new TreeItemPropertyValueFactory<>("fat"));
        sodiumColumn.setCellValueFactory(new TreeItemPropertyValueFactory<>("sodium"));
        priceColumn.setCellValueFactory(new TreeItemPropertyValueFactory<>("price"));

        TreeItem<MenuItem> menuItems = new TreeItem<>();
        treeTableView.setRoot(menuItems);
        treeTableView.setShowRoot(false);
    }

    public void initializeEditableTreeTableViewColumns(TreeTableView<MenuItem> treeTableView, TreeTableColumn<MenuItem, String> titleColumn,
                                                          TreeTableColumn<MenuItem, Double> ratingColumn, TreeTableColumn<MenuItem, Integer> caloriesColumn,
                                                          TreeTableColumn<MenuItem, Double> proteinColumn, TreeTableColumn<MenuItem, Double> fatColumn,
                                                          TreeTableColumn<MenuItem, Double> sodiumColumn, TreeTableColumn<MenuItem, Double> priceColumn, DeliveryService deliveryService) {
        initializeTreeTableViewColumns(treeTableView, titleColumn, ratingColumn, caloriesColumn, proteinColumn, fatColumn, sodiumColumn, priceColumn);

        titleColumn.setCellFactory(col -> new StringEditingCell());
        ratingColumn.setCellFactory(col -> new DoubleEditingCell(0.0));
       // caloriesColumn.setCellFactory(TextFieldTreeTableCell.forTreeTableColumn(new IntegerStringConverter()));
        caloriesColumn.setCellFactory(col -> new IntegerEditingCell(0));
        proteinColumn.setCellFactory(col -> new DoubleEditingCell(0.0));
        fatColumn.setCellFactory(col -> new DoubleEditingCell(0.0));
        sodiumColumn.setCellFactory(col -> new DoubleEditingCell(0.0));
        priceColumn.setCellFactory(col -> new DoubleEditingCell(0.0));

        titleColumn.setOnEditCommit(menuItemStringCellEditEvent -> {
            TreeItem<MenuItem> currentEditingMenuItem = treeTableView.getTreeItem(menuItemStringCellEditEvent.getTreeTablePosition().getRow());
          /*  if (menuItemStringCellEditEvent.getNewValue().isEmpty()) {
                App.createAlert("The field cannot remain empty.");
                currentEditingMenuItem.getValue().setTitle(menuItemStringCellEditEvent.getOldValue());
                return;
            }*/
            deliveryService.removeMenuProduct(currentEditingMenuItem.getValue());
            currentEditingMenuItem.getValue().setTitle(menuItemStringCellEditEvent.getNewValue());
            deliveryService.addMenuProduct(currentEditingMenuItem.getValue());
        });
        ratingColumn.setOnEditCommit(menuItemStringCellEditEvent -> {
            TreeItem<MenuItem> currentEditingMenuItem = treeTableView.getTreeItem(menuItemStringCellEditEvent.getTreeTablePosition().getRow());
            deliveryService.removeMenuProduct(currentEditingMenuItem.getValue());
            currentEditingMenuItem.getValue().setRating(menuItemStringCellEditEvent.getNewValue());
            deliveryService.addMenuProduct(currentEditingMenuItem.getValue());
        });
        caloriesColumn.setOnEditCommit(menuItemStringCellEditEvent -> {
            TreeItem<MenuItem> currentEditingMenuItem = treeTableView.getTreeItem(menuItemStringCellEditEvent.getTreeTablePosition().getRow());
            deliveryService.removeMenuProduct(currentEditingMenuItem.getValue());
            currentEditingMenuItem.getValue().setCalories(menuItemStringCellEditEvent.getNewValue());
            deliveryService.addMenuProduct(currentEditingMenuItem.getValue());
        });
        proteinColumn.setOnEditCommit(menuItemStringCellEditEvent -> {
            TreeItem<MenuItem> currentEditingMenuItem = treeTableView.getTreeItem(menuItemStringCellEditEvent.getTreeTablePosition().getRow());
            deliveryService.removeMenuProduct(currentEditingMenuItem.getValue());
            currentEditingMenuItem.getValue().setProtein(menuItemStringCellEditEvent.getNewValue());
            deliveryService.addMenuProduct(currentEditingMenuItem.getValue());
        });
        fatColumn.setOnEditCommit(menuItemStringCellEditEvent -> {
            TreeItem<MenuItem> currentEditingMenuItem = treeTableView.getTreeItem(menuItemStringCellEditEvent.getTreeTablePosition().getRow());
            deliveryService.removeMenuProduct(currentEditingMenuItem.getValue());
            currentEditingMenuItem.getValue().setFat(menuItemStringCellEditEvent.getNewValue());
            deliveryService.addMenuProduct(currentEditingMenuItem.getValue());
        });
        sodiumColumn.setOnEditCommit(menuItemStringCellEditEvent -> {
            TreeItem<MenuItem> currentEditingMenuItem = treeTableView.getTreeItem(menuItemStringCellEditEvent.getTreeTablePosition().getRow());
            deliveryService.removeMenuProduct(currentEditingMenuItem.getValue());
            currentEditingMenuItem.getValue().setSodium(menuItemStringCellEditEvent.getNewValue());
            deliveryService.addMenuProduct(currentEditingMenuItem.getValue());
        });
        priceColumn.setOnEditCommit(menuItemStringCellEditEvent -> {
            TreeItem<MenuItem> currentEditingMenuItem = treeTableView.getTreeItem(menuItemStringCellEditEvent.getTreeTablePosition().getRow());
            deliveryService.removeMenuProduct(currentEditingMenuItem.getValue());
            currentEditingMenuItem.getValue().setPrice(menuItemStringCellEditEvent.getNewValue());
            deliveryService.addMenuProduct(currentEditingMenuItem.getValue());
        });

        treeTableView.setEditable(true);
    }

    public void importProducts(TreeTableView<MenuItem> treeTableView, Collection<MenuItem> menuItems) {
        for (MenuItem menuItem : menuItems) {
            if (menuItem instanceof BaseProduct) {
                TreeItem<MenuItem> itemBaseProduct = new TreeItem<>(menuItem);
                treeTableView.getRoot().getChildren().add(itemBaseProduct);
            } else if (menuItem instanceof CompositeProduct) {
                addCompositeProductToTable(treeTableView, menuItem);
            }
        }
    }

    public void addCompositeProductToTable(TreeTableView<MenuItem> treeTableView, MenuItem menuItem) {
        TreeItem<MenuItem> itemCompositeProduct = new TreeItem<>(menuItem);
        for (MenuItem item : ((CompositeProduct) menuItem).getMenuItems()) {
            TreeItem<MenuItem> baseProduct = new TreeItem<>(item);
            itemCompositeProduct.getChildren().add(baseProduct);
        }
        treeTableView.getRoot().getChildren().add(itemCompositeProduct);
    }

}
