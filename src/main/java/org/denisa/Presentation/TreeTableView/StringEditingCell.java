package org.denisa.Presentation.TreeTableView;

import javafx.beans.binding.Bindings;
import javafx.scene.control.ContentDisplay;
import javafx.scene.input.KeyCode;
import javafx.util.StringConverter;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.control.TreeTableCell;
import org.denisa.Model.Products.MenuItem;
import javafx.scene.input.KeyEvent;

import java.util.function.UnaryOperator;

public class StringEditingCell extends TreeTableCell<MenuItem, String> {
    private TextField textField;
    private TextFormatter<String> textFormatter;

    public StringEditingCell() {
        textField = new TextField();
        UnaryOperator<TextFormatter.Change> filter = c -> {
            String newText = c.getControlNewText();

            if (newText.isEmpty()) return c;
            if (!newText.matches("([A-Z][a-z]*\\s?[-]?[&]?\\s?)+")) {
                return null;
            } else return c;

        };

        StringConverter<String> converter = new StringConverter<>() {

            @Override
            public String toString(String value) {
                return value == null ? "" : value.toString();
            }

            @Override
            public String fromString(String s) {
                if (s.matches("([A-Z][a-z]*\\s?[-]?[&]?\\s?)+")) {
                    return s;
                } else {
                    return getItem();
                }
            }
        };
        textFormatter = new TextFormatter<>(converter, "", filter);
        textField.setTextFormatter(textFormatter);

        textField.addEventFilter(KeyEvent.KEY_RELEASED, e -> {
            if (e.getCode() == KeyCode.ESCAPE) {
                cancelEdit();
            }
        });

        textField.setOnAction(e -> commitEdit(converter.fromString(textField.getText())));

        textProperty().bind(Bindings
                .when(emptyProperty())
                .then((String) null)
                .otherwise(itemProperty().asString()));

        setGraphic(textField);
        setContentDisplay(ContentDisplay.TEXT_ONLY);
    }

    @Override
    protected void updateItem(String value, boolean empty) {
        super.updateItem(value, empty);
        if (isEditing()) {
            textField.requestFocus();
            textField.selectAll();
            setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
        } else {
            setContentDisplay(ContentDisplay.TEXT_ONLY);
        }
    }

    @Override
    public void startEdit() {
        super.startEdit();
        textFormatter.setValue(getItem());
        setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
        textField.requestFocus();
        textField.selectAll();
    }

    @Override
    public void commitEdit(String newValue) {
        super.commitEdit(newValue);
        setContentDisplay(ContentDisplay.TEXT_ONLY);
    }

    @Override
    public void cancelEdit() {
        super.cancelEdit();
        setContentDisplay(ContentDisplay.TEXT_ONLY);
    }
}
