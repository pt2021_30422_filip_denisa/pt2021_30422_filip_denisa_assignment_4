package org.denisa.Presentation.TreeTableView;

import javafx.beans.binding.Bindings;
import javafx.scene.control.ContentDisplay;
import javafx.scene.input.KeyCode;
import javafx.util.StringConverter;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.control.TreeTableCell;
import org.denisa.Model.Products.MenuItem;
import javafx.scene.input.KeyEvent;

import java.util.function.UnaryOperator;

public class IntegerEditingCell extends TreeTableCell<MenuItem, Integer> {
    private TextField textField;
    private TextFormatter<Integer> textFormatter;

    public IntegerEditingCell(int min) {
        textField = new TextField();
        UnaryOperator<TextFormatter.Change> filter = c -> {
            String newText = c.getControlNewText();

            if (newText.isEmpty()) return c;
            if (!newText.matches("\\d+")) return null;

            int value = Integer.parseInt(newText);
            if (value < min) {
                return null;
            } else {
                return c;
            }
        };

        StringConverter<Integer> converter = new StringConverter<>() {

            @Override
            public String toString(Integer value) {
                return value == null ? "" : value.toString();
            }

            @Override
            public Integer fromString(String s) {
                if (s.matches("\\d+")) {
                    return Integer.valueOf(s);
                } else {
                    return getItem();
                }
            }
        };
        textFormatter = new TextFormatter<>(converter, 0, filter);
        textField.setTextFormatter(textFormatter);

        textField.addEventFilter(KeyEvent.KEY_RELEASED, e -> {
            if (e.getCode() == KeyCode.ESCAPE) {
                cancelEdit();
            }
        });

        textField.setOnAction(e -> commitEdit(converter.fromString(textField.getText())));

        textProperty().bind(Bindings
                .when(emptyProperty())
                .then((String) null)
                .otherwise(itemProperty().asString()));

        setGraphic(textField);
        setContentDisplay(ContentDisplay.TEXT_ONLY);
    }

    @Override
    protected void updateItem(Integer value, boolean empty) {
        super.updateItem(value, empty);
        if (isEditing()) {
            textField.requestFocus();
            textField.selectAll();
            setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
        } else {
            setContentDisplay(ContentDisplay.TEXT_ONLY);
        }
    }

    @Override
    public void startEdit() {
        super.startEdit();
        textFormatter.setValue(getItem());
        setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
        textField.requestFocus();
        textField.selectAll();
    }

    @Override
    public void commitEdit(Integer newValue) {
        super.commitEdit(newValue);
        setContentDisplay(ContentDisplay.TEXT_ONLY);
    }

    @Override
    public void cancelEdit() {
        super.cancelEdit();
        setContentDisplay(ContentDisplay.TEXT_ONLY);
    }
}
