package org.denisa.Presentation;

import java.io.IOException;
import java.util.List;

import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.denisa.Business.DeliveryService;
import org.denisa.Business.Validators.UserValidator;
import org.denisa.DataAccess.Serializer.UserSerializer;
import org.denisa.Model.User.Client;
import org.denisa.Model.User.Employee;
import org.denisa.Model.User.User;
import org.denisa.Model.User.UserType;

public class SignInController {
    @FXML private JFXTextField txtEmail;
    @FXML private JFXPasswordField txtPassword;
    private DeliveryService deliveryService;

    public void setDeliveryService(DeliveryService deliveryService) {
        this.deliveryService = deliveryService;
    }

    @FXML
    private void goToUserWindow() throws IOException {
        if (txtEmail.getText().isEmpty()) {
            txtEmail.setStyle("-jfx-unfocus-color: red; -jfx-focus-color: red");
            App.createAlert("Please fill in your email.");
            return;
        } else {
            txtEmail.setStyle("-jfx-unfocus-color: white; -jfx-focus-color: white");
        }
        if (txtPassword.getText().isEmpty()) {
            txtPassword.setStyle("-jfx-unfocus-color: red; -jfx-focus-color: red");
            App.createAlert("Please fill in your password");
            return;
        } else {
            txtPassword.setStyle("-jfx-unfocus-color: white; -jfx-focus-color: white");
        }

        String email = txtEmail.getText();
        String password = txtPassword.getText();

        List<User> users = new UserSerializer().deserializeList();
        User u = new UserValidator().checkIfUserExists(email, password, users);
        if (u == null) {
            App.createAlert("This email is not registered. Enter an existing email or go to registration.");
        } else {
            UserType userType = determineTypeOfUser(u);
            txtEmail.clear();
            txtPassword.clear();
            loadSceneAndSendData(userType, u);
        }
    }

    private UserType determineTypeOfUser(User u) {
        if (u instanceof Client) {
            return UserType.CLIENT;
        } else if (u instanceof Employee) {
            return UserType.EMPLOYEE;
        } else {
            return UserType.ADMINISTRATOR;
        }
    }

    private void loadSceneAndSendData(UserType userType, User user) {
        try {
            FXMLLoader loader;
            Parent root;
            switch(userType) {
                case EMPLOYEE -> {
                    loader = new FXMLLoader(getClass().getResource("/org/denisa/Presentation/Employee.fxml"));
                    root = loader.load();
                    EmployeeController employeeController = loader.getController();
                    employeeController.setEmployee(user);
                    employeeController.setEmployee(user);
                    deliveryService.addPropertyChangeListener(employeeController);
                }
                case ADMINISTRATOR -> {
                    loader = new FXMLLoader(getClass().getResource("/org/denisa/Presentation/admin.fxml"));
                    root = loader.load();
                    AdminController adminController = loader.getController();
                    adminController.setAdmin(user);
                    adminController.setDeliveryService(deliveryService);
                }
                default -> {
                    loader = new FXMLLoader(getClass().getResource("/org/denisa/Presentation/client.fxml"));
                    root = loader.load();
                    ClientController clientController = loader.getController();
                    clientController.setClient(user);
                    clientController.setDeliveryService(deliveryService);
                }
            }
            Stage stage = new Stage();
            stage.setScene(new Scene(root));
            stage.setTitle("User");
            stage.show();
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
    }

    @FXML
    private void switchToRegistration() throws IOException {
        App.setRoot("Registration");
    }
}