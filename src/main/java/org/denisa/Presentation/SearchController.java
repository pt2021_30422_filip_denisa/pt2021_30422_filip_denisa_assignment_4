package org.denisa.Presentation;

import com.jfoenix.controls.JFXTextField;
import javafx.fxml.FXML;
import javafx.scene.control.TreeTableView;
import lombok.AllArgsConstructor;
import org.denisa.Business.DeliveryService;
import org.denisa.Model.Products.BaseProduct;
import org.denisa.Model.Products.MenuItem;

import java.util.Set;

public class SearchController {

    public MenuItem constructMenuItemFromSearch(JFXTextField txtSearchTitle, JFXTextField txtSearchRating, JFXTextField txtSearchCalories, JFXTextField txtSearchProtein,
                                                 JFXTextField txtSearchFat, JFXTextField txtSearchSodium, JFXTextField txtSearchPrice) {
        MenuItem menuItem = new BaseProduct("null", -1.0, -1, -1.0, -1.0, -1.0, -1.0);
        if (txtSearchTitle.getText().isEmpty() && txtSearchRating.getText().isEmpty() && txtSearchCalories.getText().isEmpty() &&
                txtSearchProtein.getText().isEmpty() && txtSearchFat.getText().isEmpty() && txtSearchSodium.getText().isEmpty() &&
                txtSearchPrice.getText().isEmpty()) {
            return null;
        }
        if (!txtSearchTitle.getText().isEmpty()) {
            menuItem.setTitle(txtSearchTitle.getText());
        }
        if (!txtSearchRating.getText().isEmpty()) {
            menuItem.setRating(Double.valueOf(txtSearchRating.getText()));
        }
        if (!txtSearchCalories.getText().isEmpty()) {
            menuItem.setCalories(Integer.valueOf(txtSearchCalories.getText()));
        }
        if (!txtSearchProtein.getText().isEmpty()) {
            menuItem.setProtein(Double.valueOf(txtSearchProtein.getText()));
        }
        if (!txtSearchFat.getText().isEmpty()) {
            menuItem.setFat(Double.valueOf(txtSearchFat.getText()));
        }
        if (!txtSearchSodium.getText().isEmpty()) {
            menuItem.setSodium(Double.valueOf(txtSearchSodium.getText()));
        }
        if (!txtSearchPrice.getText().isEmpty()) {
            menuItem.setPrice(Double.valueOf(txtSearchPrice.getText()));
        }
        return menuItem;
    }

}
