package org.denisa.Presentation;

import java.io.IOException;
import java.util.List;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import org.denisa.Business.DeliveryService;
import org.denisa.Business.Exceptions.DuplicateEmailException;
import org.denisa.Business.Exceptions.InvalidDataException;
import org.denisa.Business.Validators.UserValidator;
import org.denisa.DataAccess.Serializer.UserSerializer;
import org.denisa.Model.User.*;

public class RegistrationController {
    @FXML private HBox hBoxButtons;
    @FXML private JFXTextField txtFirstName;
    @FXML private JFXTextField txtLastName;
    @FXML private JFXTextField txtEmail;
    @FXML private JFXPasswordField txtPassword;
    @FXML private JFXPasswordField txtConfirmPassword;
    @FXML private JFXTextField txtWorkerID;
    @FXML private Label lblWorkerId;
    @FXML private JFXButton btnClient;
    @FXML private JFXButton btnEmployee;
    @FXML private JFXButton btnAdministrator;

    private UserType userType = UserType.DEFAULT;
    private final DeliveryService deliveryService = new DeliveryService();

    @FXML
    private void register() {
        if (txtFirstName.getText().isEmpty() || txtLastName.getText().isEmpty() || txtEmail.getText().isEmpty() || txtPassword.getText().isEmpty() ||
        txtConfirmPassword.getText().isEmpty()) {
            App.createAlert("Please fill in all of the required fields.");
            return;
        }
        String firstName = txtFirstName.getText();
        String lastName = txtLastName.getText();
        String email = txtEmail.getText();
        String password = txtPassword.getText();
        User user = null;

        switch (userType) {
            case DEFAULT -> {
                App.createAlert("Please select the type of user.");
                return;
            }
            case CLIENT -> user = new Client(firstName, lastName, email, password);
            case EMPLOYEE -> {
                if (txtWorkerID.getText().isEmpty()) {
                    App.createAlert("Please fill in all of the required fields.");
                    return;
                }
                String employeeId = txtWorkerID.getText();
                user = new Employee(firstName, lastName, email, password, employeeId);
            }
            case ADMINISTRATOR -> {
                if (txtWorkerID.getText().isEmpty()) {
                    App.createAlert("Please fill in all of the required fields.");
                    return;
                }
                String administratorId = txtWorkerID.getText();
                user = new Administrator(firstName, lastName, email, password, administratorId);
            }
        }
        List<User> users = new UserSerializer().deserializeList();
        try {
            new UserValidator().checkForDuplicateEmail(user, users);
        } catch (DuplicateEmailException e) {
            App.createAlert(e.getMessage());
            return;
        }

        if (!txtPassword.getText().equals(txtConfirmPassword.getText())) {
            App.createAlert("The passwords do not match. Please insert them again.");
            txtPassword.setStyle("-jfx-unfocus-color: red; -jfx-focus-color: red");
            txtConfirmPassword.setStyle("-jfx-unfocus-color: red; -jfx-focus-color: red");
            return;
        } else {
            txtPassword.setStyle("-jfx-unfocus-color: white; -jfx-focus-color: white");
            txtConfirmPassword.setStyle("-jfx-unfocus-color: white; -jfx-focus-color: white");
        }

        try {
            new UserValidator().validate(user);
        } catch (InvalidDataException e) {
            App.createAlert(e.getMessage());
            return;
        }
        users.add(user);
        txtWorkerID.clear();
        txtPassword.clear();
        txtConfirmPassword.clear();
        txtEmail.clear();
        txtFirstName.clear();
        txtLastName.clear();
        new UserSerializer().serializeList(users);
        loadSceneAndSendData(user);
    }

    @FXML
    private void changeToClient() {
        userType = UserType.CLIENT;
        txtWorkerID.setVisible(false);
        lblWorkerId.setVisible(false);

        btnAdministrator.setStyle("-fx-background-color: black; -fx-text-fill: white;");
        btnClient.setStyle("-fx-background-color: #eddb34; -fx-text-fill: black;");
        btnEmployee.setStyle("-fx-background-color: black; -fx-text-fill: white;");
    }

    @FXML
    private void changeToEmployee() {
        userType = UserType.EMPLOYEE;
        txtWorkerID.setVisible(true);
        lblWorkerId.setVisible(true);
        lblWorkerId.setText("Employee Id");

        btnAdministrator.setStyle("-fx-background-color: black; -fx-text-fill: white;");
        btnEmployee.setStyle("-fx-background-color: #eddb34; -fx-text-fill: black;");
        btnClient.setStyle("-fx-background-color: black; -fx-text-fill: white;");
    }

    @FXML
    private void changeToAdministrator() {
        userType = UserType.ADMINISTRATOR;
        txtWorkerID.setVisible(true);
        lblWorkerId.setVisible(true);
        lblWorkerId.setText("Administrator Id");

        btnClient.setStyle("-fx-background-color: black; -fx-text-fill: white;");
        btnAdministrator.setStyle("-fx-background-color: #eddb34; -fx-text-fill: black;");
        btnEmployee.setStyle("-fx-background-color: black; -fx-text-fill: white;");
    }

    @FXML
    private void switchToSignIn() {
        loadSignInScene(deliveryService);
    }

    private void loadSceneAndSendData(User user) {
        try {
            FXMLLoader loader;
            Parent root;
            switch (userType) {
                case EMPLOYEE -> {
                    loader = new FXMLLoader(getClass().getResource("/org/denisa/Presentation/employee.fxml"));
                    root = loader.load();
                    EmployeeController employeeController = loader.getController();
                    employeeController.setEmployee(user);
                    deliveryService.addPropertyChangeListener(employeeController);
                }
                case ADMINISTRATOR -> {
                    loader = new FXMLLoader(getClass().getResource("/org/denisa/Presentation/admin.fxml"));
                    root = loader.load();
                    AdminController adminController = loader.getController();
                    adminController.setAdmin(user);
                    adminController.setDeliveryService(deliveryService);
                }
                default -> {
                    loader = new FXMLLoader(getClass().getResource("/org/denisa/Presentation/client.fxml"));
                    root = loader.load();
                    ClientController clientController = loader.getController();
                    clientController.setClient(user);
                    clientController.setDeliveryService(deliveryService);
                }
            }
            Stage stage = new Stage();
            stage.setScene(new Scene(root));
            stage.setTitle("User");
            stage.show();
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
    }

    private void loadSignInScene(DeliveryService deliveryService) {
        try {
            FXMLLoader loader;
            loader = new FXMLLoader(getClass().getResource("/org/denisa/Presentation/SignIn.fxml"));
            Parent root = loader.load();
            SignInController signInController = loader.getController();
            signInController.setDeliveryService(deliveryService);

            //App.setRoot("SignIn");
            Stage stage = new Stage();
            stage.setScene(new Scene(root));
            stage.setTitle("User");
            stage.show();
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
    }

}
