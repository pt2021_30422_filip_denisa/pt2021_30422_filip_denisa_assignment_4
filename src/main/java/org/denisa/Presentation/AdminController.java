package org.denisa.Presentation;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.controls.JFXTimePicker;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.Callback;
import org.denisa.Business.DeliveryService;
import org.denisa.Business.Exceptions.DuplicateMenuItemTitleException;
import org.denisa.Business.Exceptions.InvalidDataException;
import org.denisa.Business.Validators.MenuItemValidator;
import org.denisa.Model.Order.Order;
import org.denisa.Model.Products.BaseProduct;
import org.denisa.Model.Products.CompositeProduct;
import org.denisa.Model.Products.MenuItem;
import org.denisa.Model.User.Administrator;
import org.denisa.Model.User.Client;
import org.denisa.Model.User.User;
import org.denisa.Presentation.TreeTableView.TreeTableViewController;

import java.net.URL;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * The type Admin controller.
 */
public class AdminController {

    private final Integer ADD_ITEM = 1;
    private final Integer REMOVE_ITEM_FROM_COMPOSITE_PRODUCT = 0;
    private final Integer REMOVE_ITEM = -1;
    private final Integer MODIFY_ITEM = 2;

    @FXML private JFXButton btnBaseProduct;
    @FXML private JFXButton btnCompositeProduct;
    @FXML private JFXTextField txtNewTitle;
    @FXML private JFXTextField txtNewRating;
    @FXML private JFXTextField txtNewCalories;
    @FXML private JFXTextField txtNewProtein;
    @FXML private JFXTextField txtNewFat;
    @FXML private JFXTextField txtNewSodium;
    @FXML private JFXTextField txtNewPrice;
    @FXML private JFXTextField txtNewCompositeTitle;

    @FXML private TreeTableView<MenuItem> treeTableView;
    @FXML private TreeTableColumn<MenuItem, String> titleColumn;
    @FXML private TreeTableColumn<MenuItem, Double> ratingColumn;
    @FXML private TreeTableColumn<MenuItem, Integer> caloriesColumn;
    @FXML private TreeTableColumn<MenuItem, Double> proteinColumn;
    @FXML private TreeTableColumn<MenuItem, Double> fatColumn;
    @FXML private TreeTableColumn<MenuItem, Double> sodiumColumn;
    @FXML private TreeTableColumn<MenuItem, Double> priceColumn;
    @FXML private TreeTableColumn<MenuItem, String> addColumn;
    @FXML private TreeTableColumn<MenuItem, String> removeColumn;

    @FXML private TreeTableView<MenuItem> treeTableViewCompositeProduct;
    @FXML private TreeTableColumn<MenuItem, String> titleProductColumn;
    @FXML private TreeTableColumn<MenuItem, Double> ratingProductColumn;
    @FXML private TreeTableColumn<MenuItem, Integer> caloriesProductColumn;
    @FXML private TreeTableColumn<MenuItem, Double> proteinProductColumn;
    @FXML private TreeTableColumn<MenuItem, Double> fatProductColumn;
    @FXML private TreeTableColumn<MenuItem, Double> sodiumProductColumn;
    @FXML private TreeTableColumn<MenuItem, Double> priceProductColumn;
    @FXML private TreeTableColumn<MenuItem, String> removeProductColumn;

    @FXML private VBox searchSection;
    @FXML private JFXTextField txtSearchTitle;
    @FXML private JFXTextField txtSearchRating;
    @FXML private JFXTextField txtSearchCalories;
    @FXML private JFXTextField txtSearchProtein;
    @FXML private JFXTextField txtSearchFat;
    @FXML private JFXTextField txtSearchSodium;
    @FXML private JFXTextField txtSearchPrice;

    @FXML private TabPane tabPaneNewProduct;

    @FXML private ListView<String> listViewTimeInterval;
    @FXML private ListView<String> listViewProducts;
    @FXML private ListView<String> listViewClients;
    @FXML private ListView<String> listViewProductsInADay;
    @FXML private JFXTextField startHour, endHour, txtNumberOfTimesProducts, txtSpecifiedAmount, txtNoOfTimesClients;
    @FXML private DatePicker datePicker;

    private DeliveryService deliveryService;
    private Administrator user;

    /**
     * Sets admin.
     *
     * @param user the user
     */
    public void setAdmin(User user) {
        this.user = (Administrator) user;
    }

    /**
     * Sets delivery service.
     *
     * @param deliveryService the delivery service
     */
    public void setDeliveryService(DeliveryService deliveryService) {
        this.deliveryService = deliveryService;
        initializeTreeTableView();
        initializeTreeTableViewCompositeProduct();
    }

    private Callback<TreeTableColumn<MenuItem, String>, TreeTableCell<MenuItem, String>> createButtonInTreeTableView(String buttonText, Integer action) {
        return new Callback<>() {
            @Override
            public TreeTableCell<MenuItem, String> call(TreeTableColumn<MenuItem, String> param) {
                return new TreeTableCell<>() {
                    final JFXButton btn = new JFXButton(buttonText);

                    @Override
                    protected void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                        } else {
                            btn.setOnAction(event -> {
                                TreeItem<MenuItem> treeItem = getTreeTableView().getTreeItem(getIndex());
                                MenuItem menuItem = treeItem.getValue();
                                if (action.equals(ADD_ITEM)) {
                                    addMenuItemToCompositeProduct(menuItem);
                                } else if (action.equals(REMOVE_ITEM)){
                                    removeMenuItem(treeItem);
                                } else if (action.equals(REMOVE_ITEM_FROM_COMPOSITE_PRODUCT)) {
                                    removeMenuItemFromCompositeProduct(treeItem);
                                }
                            });
                            setGraphic(btn);
                        }
                        setText(null);
                    }
                };
            }
        };
    }

    private void removeMenuItem(TreeItem<MenuItem> treeItem) {
        deliveryService.removeMenuProduct(treeItem.getValue());
        treeTableView.getRoot().getChildren().remove(treeItem);
    }

    private void removeMenuItemFromCompositeProduct(TreeItem<MenuItem> treeItem) {
        treeTableViewCompositeProduct.getRoot().getChildren().remove(treeItem);
    }

    private void addMenuItemToCompositeProduct(MenuItem menuItem) {
        treeTableViewCompositeProduct.getRoot().getChildren().add(new TreeItem<>(menuItem));
    }

    private void addMenuItem(MenuItem menuItem) {
        treeTableView.getRoot().getChildren().add(new TreeItem<>(menuItem));
        deliveryService.addMenuProduct(menuItem);
    }

    private void initializeTreeTableView() {
        new TreeTableViewController().initializeEditableTreeTableViewColumns(treeTableView, titleColumn, ratingColumn, caloriesColumn, proteinColumn,
                fatColumn, sodiumColumn, priceColumn, deliveryService);
        removeColumn.setCellFactory(createButtonInTreeTableView("Remove", REMOVE_ITEM));
    }

    private void initializeTreeTableViewCompositeProduct() {
        new TreeTableViewController().initializeTreeTableViewColumns(treeTableViewCompositeProduct, titleProductColumn,
                ratingProductColumn, caloriesProductColumn, proteinProductColumn, fatProductColumn, sodiumProductColumn, priceProductColumn);
        removeProductColumn.setCellFactory(createButtonInTreeTableView("Remove", REMOVE_ITEM_FROM_COMPOSITE_PRODUCT));
    }

    @FXML
    private void importProducts() {
        new TreeTableViewController().importProducts(treeTableView, deliveryService.importProducts());
    }

    @FXML
    private void importModifiedProducts() {
        new TreeTableViewController().importProducts(treeTableView, deliveryService.importModifiedProducts());
    }

    @FXML
    private void addNewProduct() {
        tabPaneNewProduct.setVisible(true);
        btnCompositeProduct.setVisible(true);
        addColumn.setCellFactory(createButtonInTreeTableView("Add", ADD_ITEM));
    }

    @FXML
    private void addBaseProduct() {
        if (txtNewTitle.getText().isEmpty() || txtNewRating.getText().isEmpty() || txtNewCalories.getText().isEmpty() ||
        txtNewProtein.getText().isEmpty() || txtNewFat.getText().isEmpty() || txtNewSodium.getText().isEmpty() ||
        txtNewPrice.getText().isEmpty()) {
            App.createAlert("Please fill in all of the required fields.");
            return;
        }
        String title = txtNewTitle.getText();
        Double rating = Double.valueOf(txtNewRating.getText());
        Integer calories = Integer.valueOf(txtNewCalories.getText());
        Double protein = Double.valueOf(txtNewProtein.getText());
        Double fat = Double.valueOf(txtNewFat.getText());
        Double sodium = Double.valueOf(txtNewSodium.getText());
        Double price = Double.valueOf(txtNewPrice.getText());
        MenuItem menuItem = new BaseProduct(title, rating, calories, protein, fat, sodium, price);
        try {
            new MenuItemValidator().validate(menuItem);
        } catch (InvalidDataException e) {
            App.createAlert(e.getMessage());
            return;
        }
        try {
            new MenuItemValidator().checkForDuplicateTitle(menuItem, deliveryService.getMenu());
        } catch (DuplicateMenuItemTitleException e) {
            App.createAlert(e.getMessage());
            return;
        }
        addMenuItem(menuItem);
        txtNewTitle.clear();
        txtNewRating.clear();
        txtNewCalories.clear();
        txtNewProtein.clear();
        txtNewSodium.clear();
        txtNewPrice.clear();
        txtNewFat.clear();
    }

    @FXML
    private void addCompositeProduct() {
        if (txtNewCompositeTitle.getText().isEmpty()) {
            App.createAlert("Please fill in the title of the product you want to add.");
            txtNewCompositeTitle.setStyle("-jfx-unfocus-color: red; -jfx-focus-color: red");
            return;
        } else {
            txtNewCompositeTitle.setStyle("-jfx-unfocus-color: white; -jfx-focus-color: white");
        }

        if (treeTableViewCompositeProduct.getRoot().getChildren().size() < 2) {
            App.createAlert("You need at least two base products to create a composite product.");
            return;
        }

        CompositeProduct compositeProduct = new CompositeProduct(txtNewCompositeTitle.getText());
        for (TreeItem<MenuItem> menuItem : treeTableViewCompositeProduct.getRoot().getChildren()) {
            compositeProduct.addMenuItem(menuItem.getValue());
        }
        try {
            new MenuItemValidator().validate(compositeProduct);
        } catch (InvalidDataException e) {
            App.createAlert(e.getMessage());
            return;
        }
        try {
            new MenuItemValidator().checkForDuplicateTitle(compositeProduct, deliveryService.importModifiedProducts());
        } catch (DuplicateMenuItemTitleException e) {
            App.createAlert(e.getMessage());
            return;
        }
        compositeProduct.setProductData();
        addCompositeProduct(compositeProduct);
        txtNewCompositeTitle.clear();
        treeTableViewCompositeProduct.getRoot().getChildren().removeAll(treeTableViewCompositeProduct.getRoot().getChildren());
    }

    private void addCompositeProduct(CompositeProduct compositeProduct) {
        new TreeTableViewController().addCompositeProductToTable(treeTableView, compositeProduct);
        deliveryService.addMenuProduct(compositeProduct);
    }

    @FXML
    private void displaySearchSection() {
        searchSection.setVisible(true);
    }

    @FXML
    private void searchByCriteria() {
        MenuItem menuItem = new SearchController().constructMenuItemFromSearch(txtSearchTitle, txtSearchRating, txtSearchCalories, txtSearchProtein,
                txtSearchFat, txtSearchSodium, txtSearchPrice);
        if (menuItem == null) {
            initializeTreeTableView();
            new TreeTableViewController().importProducts(treeTableView, deliveryService.importModifiedProducts());
        } else {
            Set<MenuItem> filteredMenuItems = deliveryService.searchByCriteria(menuItem);
            initializeTreeTableView();
            new TreeTableViewController().importProducts(treeTableView, filteredMenuItems);
        }
    }

    //generate reports
    @FXML
    private void generateOrderTimeInterval() {
        listViewTimeInterval.getItems().clear();
        if (startHour == null || endHour == null) {
            App.createAlert("Please fill in the start and end hour fields.");
            return;
        }
        int startHourInt = Integer.parseInt(startHour.getText());
        int endHourInt = Integer.parseInt(endHour.getText());
        if (startHourInt < 0 || startHourInt > 23 || endHourInt < 0 || endHourInt > 23) {
            App.createAlert("The inserted hours must be in the interval [0, 23].");
            return;
        }
        if (startHourInt > endHourInt) {
            App.createAlert("The start hour must be before the end hour.");
            return;
        }
        LocalTime startTime = LocalTime.of(startHourInt, 0);
        LocalTime endTime = LocalTime.of(endHourInt, 0);

        Set<Order> orders = deliveryService.generateTimeIntervalOfTheOrders(startTime, endTime);
        List<String> ordersString = orders.stream().map(order -> order.getOrderID() + ", " + order.getOrderDate() + ", " + order.getTotalPrice())
                .collect(Collectors.toList());
        listViewTimeInterval.getItems().addAll(ordersString);

    }

    @FXML
    private void generateMostOrderedProducts() {
        listViewProducts.getItems().clear();
        if (txtNumberOfTimesProducts.getText() == null) {
            App.createAlert("Please fill in the number of times field.");
            return;
        }
        int noOfTimes = Integer.parseInt(txtNumberOfTimesProducts.getText());
        if (noOfTimes < 0) {
            App.createAlert("The number of times must be a positive number.");
            return;
        }
        Set<MenuItem> menuItems = deliveryService.generateMostOrderedProducts(noOfTimes);
        List<String> menuItemsString = menuItems.stream().map(MenuItem::getTitle).collect(Collectors.toList());
        listViewProducts.getItems().addAll(menuItemsString);
    }

    @FXML
    private void generateMostActiveClients() {
        listViewClients.getItems().clear();
        if (txtNoOfTimesClients.getText() == null || txtSpecifiedAmount.getText() == null) {
            App.createAlert("Please fill in the number of times and specified amount fields.");
            return;
        }
        int noOfTimes = Integer.parseInt(txtNoOfTimesClients.getText());
        double specifiedAmount = Double.parseDouble(txtSpecifiedAmount.getText());
        if (noOfTimes <= 0 || specifiedAmount < 0.0) {
            App.createAlert("The number of times and specified amount must be positive numbers.");
            return;
        }
        Set<Client> clients = deliveryService.generateMostLoyalCustomers(noOfTimes, specifiedAmount);
        List<String> clientsString = clients.stream().map(client -> client.getFirstName()
                + " " + client.getLastName()).collect(Collectors.toList());
        listViewClients.getItems().addAll(clientsString);
    }

    @FXML
    private void generateOrderedProductsInADay() {
        listViewProductsInADay.getItems().clear();
        if (datePicker.getValue() == null) {
            App.createAlert("Please select a date.");
            return;
        }
        if (datePicker.getValue().isAfter(LocalDate.now())) {
            App.createAlert("The chosen date cannot be after the present date.");
            return;
        }
        Map<MenuItem, Long> menuItemLongMap = deliveryService.generateProductsOrderedWithinASpecificDay(datePicker.getValue());
        List<String> menuItemFreq = menuItemLongMap.entrySet().stream().map(menuItemLongEntry -> menuItemLongEntry.getKey().getTitle()
                + " - " + menuItemLongEntry.getValue().toString()).collect(Collectors.toList());
        listViewProductsInADay.getItems().addAll(menuItemFreq);
    }

    @FXML
    private void logOut() {
        closeWindow();
    }

    private void closeWindow() {
        Stage stage = (Stage) btnCompositeProduct.getScene().getWindow();
        stage.close();
    }
}
