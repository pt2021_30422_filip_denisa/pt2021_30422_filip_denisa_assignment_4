package org.denisa.Model.User;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;

/**
 * The type Client.
 */
@NoArgsConstructor
public class Client extends User implements Serializable {
    /**
     * Instantiates a new Client.
     *
     * @param firstName the first name
     * @param lastName  the last name
     * @param email     the email
     * @param password  the password
     */
    @JsonCreator
    public Client(@JsonProperty("firstName") String firstName, @JsonProperty("lastName") String lastName, @JsonProperty("email") String email, @JsonProperty("password") String password) {
        super(firstName, lastName, email, password);
    }

    @Override
    public String toString() {
        return "Client{" +
                "firstName=" + this.getFirstName() +
                " lastName=" + this.getLastName() +
                " email=" + this.getEmail() +
                " password=" + this.getPassword() +
                '}';
    }
}
