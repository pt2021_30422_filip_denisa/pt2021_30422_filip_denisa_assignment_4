package org.denisa.Model.User;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.*;

import java.io.Serializable;

/**
 * <p>User is an abstract class that models a user that can log in the food delivery management system.</p>
 *
 * @author Denisa Filip
 */
@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME,
        property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = Administrator.class, name = "Admin"),
        @JsonSubTypes.Type(value = Employee.class, name = "Employee"),
        @JsonSubTypes.Type(value = Client.class, name = "Client")
})
public abstract class User implements Serializable {
    private String firstName;
    private String lastName;
    private String email;
    private String password;

    /**
     * <p>Constructor with all arguments for the User class.</p>
     */
    @JsonCreator
    public User(@JsonProperty("firstName") String firstName, @JsonProperty("lastName") String lastName, @JsonProperty("email") String email, @JsonProperty("password") String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
    }
}
