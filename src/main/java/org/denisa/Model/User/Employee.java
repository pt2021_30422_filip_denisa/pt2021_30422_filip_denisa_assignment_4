package org.denisa.Model.User;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.Serializable;

/**
 * The type Employee.
 */
@Getter
@Setter
public class Employee extends User implements Serializable {
    private String employeeId;

    /**
     * Instantiates a new Employee.
     *
     * @param firstName  the first name
     * @param lastName   the last name
     * @param email      the email
     * @param password   the password
     * @param employeeId the employee id
     */
    @JsonCreator
    public Employee(@JsonProperty("firstName") String firstName, @JsonProperty("lastName") String lastName, @JsonProperty("email") String email,
                    @JsonProperty("password") String password, @JsonProperty("employeeId") String employeeId) {
        super(firstName, lastName, email, password);
        this.employeeId = employeeId;
    }

    /*@Override
    public void propertyChange(PropertyChangeEvent evt) {
        System.out.println("Order was placed (" + evt.getPropertyName() + ") [old -> " +
                evt.getOldValue() + "] | [new -> " + evt.getNewValue() + "]");
    }*/

    @Override
    public String toString() {
        return "Employee {" +
                "employeeId=" + employeeId +
                " firstName=" + this.getFirstName() +
                " lastName=" + this.getLastName() +
                " email=" + this.getEmail() +
                " password=" + this.getPassword() +
                '}';
    }
}
