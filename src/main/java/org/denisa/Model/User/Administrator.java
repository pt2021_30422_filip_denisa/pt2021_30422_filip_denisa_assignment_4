package org.denisa.Model.User;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.io.Serializable;

/**
 * The type Administrator.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Administrator extends User implements Serializable {
    private String administratorId;

    /**
     * Instantiates a new Administrator.
     *
     * @param firstName       the first name
     * @param lastName        the last name
     * @param email           the email
     * @param password        the password
     * @param administratorId the administrator id
     */
    @JsonCreator
    public Administrator(@JsonProperty("firstName") String firstName, @JsonProperty("lastName") String lastName, @JsonProperty("email") String email,
                         @JsonProperty("password") String password, @JsonProperty("administratorId") String administratorId) {
        super(firstName, lastName, email, password);
        this.administratorId = administratorId;
    }

    @Override
    public String toString() {
        return "Administrator{" +
                "administratorId=" + administratorId +
                " firstName=" + this.getFirstName() +
                " lastName=" + this.getLastName() +
                " email=" + this.getEmail() +
                " password=" + this.getPassword() +
                '}';
    }
}
