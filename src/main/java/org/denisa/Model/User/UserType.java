package org.denisa.Model.User;

/**
 * The enum User type.
 */
public enum UserType {
    /**
     * Default user type.
     */
    DEFAULT,
    /**
     * Client user type.
     */
    CLIENT,
    /**
     * Administrator user type.
     */
    ADMINISTRATOR,
    /**
     * Employee user type.
     */
    EMPLOYEE
}
