package org.denisa.Model.Order;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.denisa.Model.Products.MenuItem;
import org.denisa.Model.User.Client;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * <p>Order is a class that models an order placed by a client in the food delivery management system.</p>
 *
 * @author Denisa Filip
 */
@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
public class Order implements Serializable {
    private int orderID;
    private Client client;
    private LocalDateTime orderDate;
    private double totalPrice;

    /**
     * <p>Constructor for the Order class, that takes the client that placed an order and creates a new order.</p>
     * @param client that placed an order
     */
    public Order(Client client) {
        OrderIdGenerator orderIdGenerator = new OrderIdGenerator();
        this.orderID = orderIdGenerator.computeNextOrderId();
        this.client = client;
        this.orderDate = LocalDateTime.now();
        this.totalPrice = 0.0;
    }

    /**
     * <p>Constructor with all arguments for the Order class, used when deserializing the orders from the Order.json file.</p>
     */
    @JsonCreator
    public Order(@JsonProperty("orderID") int orderID, @JsonProperty("client") Client client, @JsonProperty("orderDate")
            LocalDateTime orderDate, @JsonProperty("totalPrice") double totalPrice) {
        this.orderID = orderID;
        this.client = client;
        this.orderDate = orderDate;
        this.totalPrice = totalPrice;
    }

    /**
     * <p>Constructor for the Order class, that creates an Order object from a string.</p>
     * @param key - string which contains the string format of an Order.
     */
    public Order(String key) {
        Order order = fromString(key);
        this.orderID = order.getOrderID();
        this.client = order.getClient();
        this.orderDate = order.getOrderDate();
        this.totalPrice = order.getTotalPrice();
    }

    /**
     * <p>Computes the total price of the order, by summing up the individual price of each ordered product.</p>
     * @param menuItemList - contains the ordered menu items
     */
    public void computeTotalPriceOfOrder(List<MenuItem> menuItemList) {
        this.totalPrice = menuItemList.stream().mapToDouble(MenuItem::computePrice).sum();
    }

    /**
     * <p>Creates an Order object from a String, using regex.</p>
     * @param s - string which contains the string format of an order.
     * @return an order object
     */
    public Order fromString(String s) {
        final String orderPattern = "Order\\{orderID=(\\d+), client=Client\\{firstName=([A-Za-z-]+) lastName=([A-Za-z-]+) " +
                "email=([A-Za-z0-9-_:?&.@]+) password=([A-Za-z0-9-_?:!.@]+)}, orderDate=([0-9-T:.]+), totalPrice=([0-9.]+)}";
        Order order = new Order();
        Client c = new Client();

        Pattern p = Pattern.compile(orderPattern);
        Matcher m = p.matcher(s);
        if (m.find()) {
            order.setOrderID(Integer.parseInt(m.group(1)));
            c.setFirstName(m.group(2));
            c.setLastName(m.group(3));
            c.setEmail(m.group(4));
            c.setPassword(m.group(5));
            order.setClient(c);
            order.setOrderDate(LocalDateTime.parse(m.group(6)));
            order.setTotalPrice(Double.parseDouble(m.group(7)));
        }
        return order;
    }

    @Override
    public String toString() {
        return "Order{" +
                "orderID=" + orderID +
                ", client=" + client +
                ", orderDate=" + orderDate +
                ", totalPrice=" + totalPrice +
                '}';
    }
}
