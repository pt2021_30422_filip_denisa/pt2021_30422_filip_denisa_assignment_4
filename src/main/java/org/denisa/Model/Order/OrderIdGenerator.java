package org.denisa.Model.Order;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.denisa.DataAccess.Serializer.OrderSerializer;
import org.denisa.Model.Products.MenuItem;

import java.util.List;
import java.util.Map;

/**
 * <p>OrderIdGenerator is responsible for generating the id of an order, based on the orders placed before it.</p>
 *
 * @author Denisa Filip
 */
@Getter
@Setter
@NoArgsConstructor
public class OrderIdGenerator {
    private int id;

    /**
     * <p>Function that computes the orderId of the next order</p>
     *
     * @return the integer orderId of an order
     */
    public int computeNextOrderId() {
        OrderSerializer orderSerializer = new OrderSerializer();
        Map<Order, List<MenuItem>> orders = orderSerializer.deserializeMap();
        if (orders == null || orders.isEmpty()) {
            return 0;
        }
        return orders.size();
    }
}
