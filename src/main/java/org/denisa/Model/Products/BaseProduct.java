package org.denisa.Model.Products;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.io.Serializable;

/**
 * The type Base product.
 */
@Setter
@Getter
@NoArgsConstructor
public class BaseProduct extends MenuItem implements Serializable {

    /**
     * Instantiates a new BaseProduct.
     *
     * @param title    the title
     * @param rating   the rating
     * @param calories the calories
     * @param protein  the protein
     * @param fat      the fat
     * @param sodium   the sodium
     * @param price    the price
     */
    @JsonCreator
    public BaseProduct(@JsonProperty("title") String title, @JsonProperty("rating") Double rating,
                       @JsonProperty("calories") Integer calories, @JsonProperty("protein") Double protein,
                       @JsonProperty("fat") Double fat, @JsonProperty("sodium") Double sodium, @JsonProperty("price") Double price) {
        super(title, rating, calories, protein, fat, sodium, price);
    }

    @Override
    public Double computeRating() {
        return this.getRating();
    }

    @Override
    public Integer computeCalories() {
        return this.getCalories();
    }

    @Override
    public Double computeProtein() {
        return this.getProtein();
    }

    @Override
    public Double computeFat() {
        return this.getFat();
    }

    @Override
    public Double computeSodium() {
        return this.getSodium();
    }

    @Override
    public Double computePrice() {
        return this.getPrice();
    }


}
