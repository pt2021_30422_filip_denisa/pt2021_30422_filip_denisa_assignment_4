package org.denisa.Model.Products;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.*;

import java.io.Serializable;

/**
 * <p>MenuItem is an abstract class that models a menu item that can be found on the menu of the food delivery management system.</p>
 *
 * @author Denisa Filip
 */
@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME,
        property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = BaseProduct.class, name = "BaseProduct"),
        @JsonSubTypes.Type(value = CompositeProduct.class, name = "CompositeProduct")
})
public abstract class MenuItem implements Serializable {

    private String title;
    private Double rating;
    private Integer calories;
    private Double protein;
    private Double fat;
    private Double sodium;
    private Double price;

    /**
     * <p>Constructor with all arguments for the MenuItem class - used when creating a BaseProduct child class.</p>
     */
    @JsonCreator
    public MenuItem(@JsonProperty("title") String title, @JsonProperty("rating") Double rating, @JsonProperty("calories") Integer calories, @JsonProperty("protein") Double protein, @JsonProperty("fat") Double fat, @JsonProperty("sodium") Double sodium, @JsonProperty("price") Double price) {
        this.title = title;
        this.rating = rating;
        this.calories = calories;
        this.protein = protein;
        this.fat = fat;
        this.sodium = sodium;
        this.price = price;
    }

    /**
     * <p>Constructor with all arguments for the MenuItem class - used when creating a CompositeProduct child object.</p>
     */
    @JsonCreator
    public MenuItem(@JsonProperty("title")String title) {
        this.title = title;
    }

    /**
     * <p>Computes the rating of a menu item. Function that is implemented by each child class, with the Composite design pattern.</p>
     * @return the rating of a product
     */
    public abstract Double computeRating();

    /**
     * <p>Computes the calories of a menu item. Function that is implemented by each child class, with the Composite design pattern.</p>
     * @return the calories of a product
     */
    public abstract Integer computeCalories();

    /**
     * <p>Computes the protein of a menu item. Function that is implemented by each child class, with the Composite design pattern.</p>
     * @return the protein of a product
     */
    public abstract Double computeProtein();

    /**
     * <p>Computes the fat of a menu item. Function that is implemented by each child class, with the Composite design pattern.</p>
     * @return the fat of a product
     */
    public abstract Double computeFat();

    /**
     * <p>Computes the sodium of a menu item. Function that is implemented by each child class, with the Composite design pattern.</p>
     * @return the sodium of a product
     */
    public abstract Double computeSodium();

    /**
     * <p>Computes the price of a menu item. Function that is implemented by each child class, with the Composite design pattern.</p>
     * @return the price of a product
     */
    public abstract Double computePrice();

    @Override
    public String toString() {
        return "MenuItem{" +
                "title='" + title + '\'' +
                ", rating=" + rating +
                ", calories=" + calories +
                ", protein=" + protein +
                ", fat=" + fat +
                ", sodium=" + sodium +
                ", price=" + price +
                '}';
    }

    /**
     * Function that prints the menu item details in a more readable manner - used when creating the .pdf bill.
     * @return the String containing the details of the menu item
     */
    public String productDetailsToStringForPDF() {
        return "     rating: " + rating + ", calories: " + calories + ", protein: " + protein + ", fat: " +
                fat + ", sodium: " + sodium;
    }
}
