package org.denisa.Model.Products;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * The type Composite product.
 */
@Getter
@Setter
public class CompositeProduct extends MenuItem implements Serializable {
    private Set<MenuItem> menuItems;

    /**
     * Instantiates a new Composite product.
     *
     * @param title the title
     */
    public CompositeProduct(String title) {
        super(title);
        menuItems = new LinkedHashSet<>();
    }

    /**
     * Instantiates a new Composite product.
     *
     * @param title     the title
     * @param menuItems the menu items
     */
    public CompositeProduct(String title, LinkedHashSet<MenuItem> menuItems) {
        this.menuItems = menuItems;
        this.setTitle(title);
        this.setRating(this.computeRating());
        this.setCalories(this.computeCalories());
        this.setProtein(this.computeProtein());
        this.setFat(this.computeFat());
        this.setSodium(this.computeSodium());
        this.setPrice(this.computePrice());
    }

    /**
     * Instantiates a new Composite product.
     *
     * @param title     the title
     * @param rating    the rating
     * @param calories  the calories
     * @param protein   the protein
     * @param fat       the fat
     * @param sodium    the sodium
     * @param price     the price
     * @param menuItems the menu items
     */
    @JsonCreator
    public CompositeProduct(@JsonProperty("title")String title, @JsonProperty("rating") Double rating,
                            @JsonProperty("calories") Integer calories, @JsonProperty("protein") Double protein,
                            @JsonProperty("fat") Double fat, @JsonProperty("sodium") Double sodium, @JsonProperty("price") Double price,
                            @JsonProperty("menuItems") Set<MenuItem> menuItems) {
        super(title, rating, calories, protein, fat, sodium, price);
        this.menuItems = menuItems;
    }

    /**
     * Sets product data.
     */
    public void setProductData() {
        this.setRating(this.computeRating());
        this.setCalories(this.computeCalories());
        this.setProtein(this.computeProtein());
        this.setFat(this.computeFat());
        this.setSodium(this.computeSodium());
        this.setPrice(this.computePrice());
    }

    /**
     * Add menu item.
     *
     * @param menuItem the menu item
     */
    @JsonSetter
    public void addMenuItem(MenuItem menuItem) {
        menuItems.add(menuItem);
    }

    @Override
    public Double computePrice() {
        return menuItems.stream().mapToDouble(MenuItem::computePrice).sum();
    }

    @Override
    public Double computeRating() {
        return menuItems.stream().mapToDouble(MenuItem::computeRating).sum() / menuItems.size();
    }

    @Override
    public Integer computeCalories() {
        return menuItems.stream().mapToInt(MenuItem::computeCalories).sum();
    }

    @Override
    public Double computeProtein() {
        return menuItems.stream().mapToDouble(MenuItem::computeProtein).sum();
    }

    @Override
    public Double computeFat() {
        return menuItems.stream().mapToDouble(MenuItem::computeFat).sum();
    }

    @Override
    public Double computeSodium() {
        return menuItems.stream().mapToDouble(MenuItem::computeSodium).sum();
    }
}
