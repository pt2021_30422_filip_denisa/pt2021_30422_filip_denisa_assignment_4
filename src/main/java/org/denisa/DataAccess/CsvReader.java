package org.denisa.DataAccess;

import com.fasterxml.jackson.annotation.JsonSetter;
import lombok.AllArgsConstructor;
import org.denisa.DataAccess.Serializer.Serializer;
import org.denisa.Model.Products.BaseProduct;
import org.denisa.Model.Products.MenuItem;

import java.io.*;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * <p>CsvReader is responsible for parsing data from the products.csv file.</p>
 *
 * @author Denisa Filip
 */
@AllArgsConstructor
public class CsvReader {
    /**Constant <code>COMMA</code> separator for the .csv file*/
    private static final String COMMA = ",";
    /** Constant <code>LOGGER</code> */
    protected static final Logger LOGGER = Logger.getLogger(Serializer.class.getName());

    /**
     * Function that takes a string and returns a base product.
     */
    private final Function<String, BaseProduct> mapToItem = (line) -> {
        String[] string = line.split(COMMA);
        String title = string[0];
        Double rating = Double.valueOf(string[1]);
        Integer calories = Integer.valueOf(string[2]);
        Double protein = Double.valueOf(string[3]);
        Double fat = Double.valueOf(string[4]);
        Double sodium = Double.valueOf(string[5]);
        Double price = Double.valueOf(string[6]);

        return new BaseProduct(title, rating, calories, protein, fat, sodium, price);
    };

    /**
     * Function that ensures the fact that the objects extracted from the .csv file are unique - it dismisses the duplicate items.
     * @param keyExtractor - key extractor
     * @param <T> - class for which it checks
     * @return if the element should be added or not
     */
    public static <T> Predicate<T> distinctByKey(Function<? super T, ?> keyExtractor) {
        Set<Object> seen = ConcurrentHashMap.newKeySet();
        return t -> seen.add(keyExtractor.apply(t));
    }

    /**
     * Function that processes the products.csv file, by mapping each row to a Base Product object.
     * @param csvFilePath - path of the .csv file
     * @return a LinkedHashSet containing all the extracted menu items from the .csv file
     */
    @JsonSetter
    public LinkedHashSet<MenuItem> processCSVFile(String csvFilePath) {
        LinkedHashSet<MenuItem> menuItems;
        try {
            File csvFile = new File(csvFilePath);
            InputStream inputStream = new FileInputStream(csvFile);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            menuItems = bufferedReader.lines().skip(1).map(mapToItem).filter(distinctByKey(MenuItem::getTitle)).collect(Collectors.toCollection(LinkedHashSet::new));
            bufferedReader.close();
            return menuItems;
        } catch (IOException e) {
            LOGGER.log(Level.WARNING, "CsvReader: processCSVFile " + e.getMessage());
        }
        return null;
    }

}
