package org.denisa.DataAccess.Serializer;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * AbstractSerializer is an abstract class that implements the Serializer interface. It defines methods for serializing/deserializing
 * objects and Collections of type T from .json file, using the Jackson library.
 *
 * @param <T> - type of the object which can be serialized/deserialized
 *
 * @author Denisa Filip
 */
public abstract class AbstractSerializer<T> implements Serializer<T> {
    /** Constant <code>LOGGER</code> */
    protected static final Logger LOGGER = Logger.getLogger(Serializer.class.getName());
    protected final ObjectMapper mapper;
    protected File file;

    public AbstractSerializer(File file) {
        this.file = file;
        mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.INDENT_OUTPUT, true);
    }


    @Override
    public void serializeObject(T object) {
        try {
            mapper.writeValue(file, object);
        } catch(JsonGenerationException e) {
            LOGGER.log(Level.WARNING, "Serializer: serializeObject - JsonGenerationException: " + e.getMessage());
        } catch(JsonMappingException e) {
            LOGGER.log(Level.WARNING, "Serializer: serializeObject - JsonMappingException: " + e.getMessage());
        } catch(IOException e) {
            LOGGER.log(Level.WARNING, "Serializer: serializeObject - IOException: " + e.getMessage());
        }
    }
    @Override
    public abstract void serializeList(List<T> objects);
    @Override
    public abstract T deserializeObject();
    @Override
    public abstract List<T> deserializeList();
}
