package org.denisa.DataAccess.Serializer;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import lombok.NonNull;
import org.denisa.DataAccess.Serializer.CustomDeserializer.OrderJacksonModule;
import org.denisa.Model.Order.Order;
import org.denisa.Model.Products.MenuItem;

import java.io.File;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * OrderSerializer is a special type of Serializer, that serializes/deserializes a map of type Map<Order, List<MenuItem>>. It uses a custom
 * key deserializer, that allows it to deserialize a Map with a key of class Order.
 *
 * @author Denisa Filip
 */
public class OrderSerializer {

    @NonNull
    private final ObjectMapper mapper = new ObjectMapper();
    @NonNull
    private final File file = new File("Order.json");
    @NonNull
    protected static final Logger LOGGER = Logger.getLogger(OrderSerializer.class.getName());

    /**
     * Function that serializes a map to a .json file
     *
     * @param objects to be serialized
     */
    public void serializeMap(Map<Order, List<MenuItem>> objects) {
        try {
            mapper.configure(SerializationFeature.INDENT_OUTPUT, true);
            mapper.writerFor(new TypeReference<Map<Order, List<MenuItem>>>() {}).writeValue(file, objects);
        } catch(JsonGenerationException e) {
            LOGGER.log(Level.WARNING, "OrderSerializer:  serializeMap - JsonGenerationException: " + e.getMessage());
        } catch(JsonMappingException e) {
            LOGGER.log(Level.WARNING, "OrderSerializer:  serializeMap - JsonMappingException: " + e.getMessage());
        } catch(IOException e) {
            LOGGER.log(Level.WARNING, "OrderSerializer:  serializeMap - IOException: " + e.getMessage());
        }
    }

    /**
     * Function that deserializes a map from a .json file.
     *
     * @return map that was deserialized
     */
    public Map<Order, List<MenuItem>> deserializeMap() {
        try {
            mapper.configure(SerializationFeature.INDENT_OUTPUT, true);
            mapper.registerModule(new OrderJacksonModule());
            if (file.length() == 0) {
                return new LinkedHashMap<>();
            }
            return mapper.readValue(file, new TypeReference<>() {});
        } catch (JsonParseException e) {
            LOGGER.log(Level.WARNING, "OrderSerializer: deserializeMap - JsonParseException: " + e.getMessage());
        } catch (JsonMappingException e) {
            LOGGER.log(Level.WARNING, "OrderSerializer: deserializeMap - JsonMappingException:  " + e.getMessage());
        } catch (IOException e) {
            LOGGER.log(Level.WARNING, "OrderSerializer: deserializeMap - IOException: " + e.getMessage());
        }
        return null;
    }
}
