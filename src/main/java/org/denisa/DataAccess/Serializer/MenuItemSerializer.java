package org.denisa.DataAccess.Serializer;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import org.denisa.Model.Products.MenuItem;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.logging.Level;

/**
 * MenuItemSerializer extends the AbstractSerializer abstract class and instantiates the type T with the MenuItem class. It is
 * responsible for serializing/deserializing menu items from .json files.
 *
 * @author Denisa Filip
 */
public class MenuItemSerializer extends AbstractSerializer<MenuItem> {

    /**
     * Constructor with no arguments for the MenuItemSerializer class.
     */
    public MenuItemSerializer() {
        super(new File("MenuItem.json"));
    }

    @Override
    public void serializeList(List<MenuItem> objects) {
        try {
            mapper.writerFor(new TypeReference<ArrayList<MenuItem>>() {
            }).writeValue(file, objects);
        } catch(JsonGenerationException e) {
            LOGGER.log(Level.WARNING, "MenuItemSerializer: serializeList - JsonGenerationException: " + e.getMessage());
        } catch(JsonMappingException e) {
            LOGGER.log(Level.WARNING, "MenuItemSerializer: serializeList - JsonMappingException: " + e.getMessage());
        } catch(IOException e) {
            LOGGER.log(Level.WARNING, "MenuItemSerializer: serializeList - IOException: " + e.getMessage());
        }
    }

    public void serializeSet(Set<MenuItem> objects) {
        try {
            mapper.writerFor(new TypeReference<Set<MenuItem>>() {}).writeValue(file, objects);
        } catch(JsonGenerationException e) {
            LOGGER.log(Level.WARNING, "MenuItemSerializer: serializeSet - JsonGenerationException: " + e.getMessage());
        } catch(JsonMappingException e) {
            LOGGER.log(Level.WARNING, "MenuItemSerializer: serializeSet - JsonMappingException: " + e.getMessage());
        } catch(IOException e) {
            LOGGER.log(Level.WARNING, "MenuItemSerializer: serializeSet - IOException: " + e.getMessage());
        }
    }

    public Set<MenuItem> deserializeSet() {
        try {
            if (file.length() == 0) {
                return new LinkedHashSet<>();
            }
            return mapper.readValue(file, new TypeReference<LinkedHashSet<MenuItem>>(){});
        } catch (JsonParseException e) {
            LOGGER.log(Level.WARNING, "MenuItemSerializer: deserializeSet - JsonParseException: " + e.getMessage());
        } catch (JsonMappingException e) {
            LOGGER.log(Level.WARNING, "MenuItemSerializer: deserializeSet - JsonMappingException:  " + e.getMessage());
        } catch (IOException e) {
            LOGGER.log(Level.WARNING, "MenuItemSerializer: deserializeSet - IOException: " + e.getMessage());
        }
        return null;
    }

    @Override
    public MenuItem deserializeObject() {
        try {
            if (file.length() == 0) {
                return null;
            }
            return mapper.readValue(file, MenuItem.class);
        } catch (JsonParseException e) {
            LOGGER.log(Level.WARNING, "MenuItemSerializer: deserializeObject - JsonParseException: " + e.getMessage());
        } catch (JsonMappingException e) {
            LOGGER.log(Level.WARNING, "MenuItemSerializer: deserializeObject - JsonMappingException:  " + e.getMessage());
        } catch (IOException e) {
            LOGGER.log(Level.WARNING, "MenuItemSerializer: deserializeObject - IOException: " + e.getMessage());
        }
        return null;
    }

    @Override
    public List<MenuItem> deserializeList() {
        try {
            if (file.length() == 0) {
                return new ArrayList<>();
            }
            return mapper.readValue(file, new TypeReference<ArrayList<MenuItem>>(){});
        } catch (JsonParseException e) {
            LOGGER.log(Level.WARNING, "MenuItemSerializer: deserializeList - JsonParseException: " + e.getMessage());
        } catch (JsonMappingException e) {
            LOGGER.log(Level.WARNING, "MenuItemSerializer: deserializeList - JsonMappingException:  " + e.getMessage());
        } catch (IOException e) {
            LOGGER.log(Level.WARNING, "MenuItemSerializer: deserializeList - IOException: " + e.getMessage());
        }
        return null;
    }
}
