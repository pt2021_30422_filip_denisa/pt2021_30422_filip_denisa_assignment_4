package org.denisa.DataAccess.Serializer.CustomDeserializer;

import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.KeyDeserializer;
import org.denisa.Model.Order.Order;

/**
 * OrderKeyDeserializer is a custom key deserializer that allows the deserialization of a Map that has a key which is an Order object.
 *
 * @author Denisa Filip
 */
public class OrderKeyDeserializer extends KeyDeserializer {
    /**
     * Function that deserializes a key of class Order.
     * @param s - string containing the Order object
     * @param deserializationContext - deserialization context
     * @return an Order object constructed from the string s
     */
    @Override
    public Order deserializeKey(String s, DeserializationContext deserializationContext) {
        return new Order(s);
    }
}
