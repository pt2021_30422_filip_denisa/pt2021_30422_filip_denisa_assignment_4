package org.denisa.DataAccess.Serializer.CustomDeserializer;

import com.fasterxml.jackson.databind.module.SimpleModule;
import org.denisa.Model.Order.Order;

/**
 * OrderJacksonModule defines a new Jackson module for the Order class, adding a the custom key deserializer OrderKeyDeserializer.
 *
 * @author Denisa Filip
 */
public class OrderJacksonModule extends SimpleModule {

    /**
     * Constructor with no arguments for the OrderJacksonModule class.
     */
    public OrderJacksonModule() {
        addKeyDeserializer(Order.class, new OrderKeyDeserializer());
    }
}
