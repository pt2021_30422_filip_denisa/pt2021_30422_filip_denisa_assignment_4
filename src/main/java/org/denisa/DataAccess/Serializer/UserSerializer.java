package org.denisa.DataAccess.Serializer;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import org.denisa.Model.User.User;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

/**
 * <p>UserSerializer extends the AbstractSerializer abstract class and instantiates the type T with the User class. It is responsible for
 * serializing/deserializing users from .json files.</p>
 *
 * @author Denisa Filip
 */
public class UserSerializer extends AbstractSerializer<User> {

    /**
     * Constructor with no arguments for the UserSerializer class.
     */
    public UserSerializer() {
        super(new File("User.json"));
    }

    @Override
    public void serializeList(List<User> objects) {
        try {
            mapper.writerFor(new TypeReference<ArrayList<User>>() {
            }).writeValue(file, objects);
        } catch(JsonGenerationException e) {
            LOGGER.log(Level.WARNING, "UserSerializer: serializeList - JsonGenerationException: " + e.getMessage());
        } catch(JsonMappingException e) {
            LOGGER.log(Level.WARNING, "UserSerializer: serializeList - JsonMappingException: " + e.getMessage());
        } catch(IOException e) {
            LOGGER.log(Level.WARNING, "UserSerializer: serializeList - IOException: " + e.getMessage());
        }
    }

    @Override
    public User deserializeObject() {
        try {
            if (file.length() == 0) {
                return null;
            }
            return mapper.readValue(file, User.class);
        } catch (JsonParseException e) {
            LOGGER.log(Level.WARNING, "UserSerializer: deserializeObject - JsonParseException: " + e.getMessage());
        } catch (JsonMappingException e) {
            LOGGER.log(Level.WARNING, "UserSerializer: deserializeObject - JsonMappingException:  " + e.getMessage());
        } catch (IOException e) {
            LOGGER.log(Level.WARNING, "UserSerializer: deserializeObject - IOException: " + e.getMessage());
        }
        return null;
    }

    @Override
    public List<User> deserializeList() {
        try {
            if (file.length() == 0) {
                return new ArrayList<>();
            }
            return mapper.readValue(file, new TypeReference<ArrayList<User>>(){});
        } catch (JsonParseException e) {
            LOGGER.log(Level.WARNING, "UserSerializer: deserializeList - JsonParseException: " + e.getMessage());
        } catch (JsonMappingException e) {
            LOGGER.log(Level.WARNING, "UserSerializer: deserializeList - JsonMappingException:  " + e.getMessage());
        } catch (IOException e) {
            LOGGER.log(Level.WARNING, "UserSerializer: deserializeList - IOException: " + e.getMessage());
        }
        return null;
    }
}
