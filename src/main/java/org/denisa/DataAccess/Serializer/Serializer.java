package org.denisa.DataAccess.Serializer;

import java.util.*;

/**
 * Serializer is an interface that defines the methods for serializing/deserializing objects or collections of objects of type T
 * from .json files, using the Jackson library.
 *
 * @param <T> - type of the object which can be serialized/deserialized
 *
 * @author Denisa Filip
 */

public interface Serializer<T> {

    /**
     * Function that serializes an object of type T.
     * @param object to be serialized
     */
    void serializeObject(T object);

    /**
     * Function that serializes a list of objects of type T.
     *
     * @param objects to be serialized
     */
    void serializeList(List<T> objects);

    /**
     * Function that deserializes an object of type T.
     * @return the deserialized object, of type T
     */
    T deserializeObject();

    /**
     * Function that deserializes a list of objects of type T.
     * @return the deserialized list, containing objects of type T
     */
    List<T> deserializeList();

}
