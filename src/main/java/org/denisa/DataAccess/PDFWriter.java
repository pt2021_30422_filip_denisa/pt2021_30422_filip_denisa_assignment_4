package org.denisa.DataAccess;

import lombok.Getter;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.denisa.Model.Order.Order;
import org.denisa.Model.Products.MenuItem;

import java.io.IOException;
import java.time.format.DateTimeFormatter;
import java.util.List;

/**
 * PDFWriter is a class responsible for writing into a .pdf file the data of the bill created when an order is placed by a client.
 *
 * @author Denisa Filip
 * @version $Id: $Id
 */
@Getter
public class PDFWriter {
    private Order order;
    private List<MenuItem> menuItems;
    private PDPage currentPage = new PDPage();

    /**
     * <p>Constructor for PDFWriter.</p>
     */
    public PDFWriter() {
    }

    /**
     * <p>Constructor for PDFWriter.</p>
     *
     * @param menuItems a DeliveryService object.
     * @param order an Order object.
     */

    public PDFWriter(Order order, List<MenuItem> menuItems) {
        this.menuItems = menuItems;
        this.order = order;
    }

    /**
     *<p> writeBillToPDF is responsible for writing the bill of an order placed by a client. </p>
     */
    public void writeBillToPDF() {
        try (PDDocument doc = new PDDocument()) {

            doc.addPage(currentPage);
            PDRectangle mediaBox = currentPage.getMediaBox();
            try (PDPageContentStream contentStream = new PDPageContentStream(doc, currentPage)) {
                contentStream.beginText();
                contentStream.setLeading(14.5f);
                centerTitle(contentStream, mediaBox);

                writeClientData(contentStream);
                writeOrderData(doc, contentStream);
            }

            doc.save("src/main/resources/org/denisa/pdfs/bill" + order.getOrderID() + ".pdf");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void writeClientData(PDPageContentStream contentStream) throws IOException {
        contentStream.setFont(PDType1Font.TIMES_ROMAN, 14);
        contentStream.newLine();
        contentStream.newLine();
        contentStream.newLine();
        contentStream.showText("Details of the client: ");
        contentStream.newLine();
        contentStream.showText("    Name: " + order.getClient().getFirstName() + " " + order.getClient().getLastName());
        contentStream.newLine();
        contentStream.showText("    Email: " + order.getClient().getEmail());
        contentStream.newLine();
        contentStream.newLine();
    }

    private void writeOrderData(PDDocument doc, PDPageContentStream contentStream) throws IOException {
        contentStream.setFont(PDType1Font.TIMES_ROMAN, 15);
        contentStream.showText("Details of the ordered menu items: ");
        contentStream.newLine();
        contentStream.newLine();
        int indexOfCurrentMenuItem = 1;
        for (MenuItem menuItem : this.menuItems) {
            if (indexOfCurrentMenuItem % 10 == 0) {
                contentStream.endText();
                this.currentPage = new PDPage();
                doc.addPage(currentPage);
                contentStream.close();
                contentStream = new PDPageContentStream(doc, currentPage);
                contentStream.beginText();
                contentStream.setLeading(14.5f);
                contentStream.newLineAtOffset(25, 700);
            }
            contentStream.setFont(PDType1Font.TIMES_BOLD, 15);
            contentStream.showText(menuItem.getTitle() + ": ");
            contentStream.newLine();
            contentStream.setFont(PDType1Font.TIMES_ROMAN, 14);
            contentStream.showText(menuItem.productDetailsToStringForPDF());
            contentStream.newLine();
            contentStream.setFont(PDType1Font.TIMES_BOLD, 14);
            contentStream.showText("     price: " + menuItem.getPrice());
            contentStream.newLine();
            contentStream.showText("--------------------------------------------------------------------------------------------");
            contentStream.newLine();
            indexOfCurrentMenuItem++;
        }
        contentStream.newLine();
        contentStream.setFont(PDType1Font.TIMES_ROMAN, 15);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        contentStream.showText("Date and time of the order: " + order.getOrderDate().format(formatter));
        contentStream.newLine();

        contentStream.newLine();

        writeTotalPriceOfOrder(contentStream, order.getTotalPrice());
        contentStream.endText();
        contentStream.close();

    }

    private void writeTotalPriceOfOrder(PDPageContentStream contentStream, double totalPrice) throws IOException {
        contentStream.setFont(PDType1Font.TIMES_BOLD, 18);
        contentStream.showText("Total price of bill: " + totalPrice);
    }

    private void centerTitle(PDPageContentStream contentStream, PDRectangle mediaBox) throws IOException {
        String title = "ORDER NUMBER " + order.getOrderID();
        PDFont font = PDType1Font.TIMES_BOLD;
        int marginTop = 30;
        int fontSize = 22;

        float titleWidth = font.getStringWidth(title) / 1000 * fontSize;
        float titleHeight = font.getFontDescriptor().getFontBoundingBox().getHeight() / 1000 * fontSize;

        float startX = (mediaBox.getWidth() - titleWidth) / 2;
        float startY = mediaBox.getHeight() - marginTop - titleHeight;

        contentStream.setFont(font, fontSize);
        contentStream.newLineAtOffset(startX, startY);

        contentStream.showText(title);
        contentStream.newLine();
        contentStream.newLineAtOffset(-startX+25, -15);
    }

}
