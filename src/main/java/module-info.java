module org.denisa {
    requires javafx.controls;
    requires javafx.fxml;
    requires static lombok;
    requires com.fasterxml.jackson.databind;
    requires java.logging;
    requires java.desktop;
    requires com.jfoenix;
    requires org.apache.pdfbox;

    opens org.denisa.Model.Order to com.fasterxml.jackson.databind;
    opens org.denisa.Model.User to com.fasterxml.jackson.databind;
    opens org.denisa.Model.Products to com.fasterxml.jackson.databind;
    opens org.denisa.Presentation to javafx.fxml;
    exports org.denisa.Model.Order;
    exports org.denisa.Model.User;
    exports org.denisa.Model.Products;
    exports org.denisa.Presentation;
    exports org.denisa.Business;
    exports org.denisa.DataAccess;

}